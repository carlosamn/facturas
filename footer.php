<!--Formulario de contacto-->

<?php 

if (isset($_POST['sendMsg'])) {
          $asunto=$_POST['asunto'];
          $mensaje=$_POST['mensaje'];     
          $userMensaje= new User($_POST['nombre'],'000000000',$_POST['email'], 'secret', 'aux' );
          if (sendMail($userMensaje, $asunto, $mensaje,'special')) {
            $msgConfirm="Su mensaje ha sido enviado exitosamente";
          }else{
            $msgConfirm="Lo sentimos no se pudo realizar la accion por favor comuniquese con el administrador del sitio";
          }
      }
 ?>

<div id="formContacto" class="" style="display: none;">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <strong>Formulario de contacto</strong>
    </div>
    <div class="panel-body">

      <form  role="form" action="index.php" method="POST">
                                    
                              <div class="form-group">
                  <label>Email:</label>
                  <input name="email" type="text" class="form-control" readonly value="<?php echo $_SESSION['email'] ?>">
                </div>
                
                <div class="form-group">
                  <label >Nombre:</label>
                  <input readonly name="nombre" type="text" class="form-control" value="<?php echo $_SESSION['username']; ?>">
                  </div>

                  <div class="form-group">
                  <label>Asunto:</label>
                  <input required name="asunto" type="text" class="form-control" size="200" placeholder="Asunto">
                  </div>

                  <div class="form-group">
                  <label>Mensaje:</label>
                  <textarea required name="mensaje" class="form-control" placeholder="Detalle del requerimiento o comentarios"></textarea>
                  </div>
                            <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="row">
                                      <div class="col-lg-6"><input name="sendMsg" type="submit" class="btn btn-info btn-block" value="Enviar mensaje"> </div>
                                      <div class="col-lg-6"><button type="button" class="btn btn-danger btn-block" id="btnCancelar">Cancelar</button> </div>
                                    </div>
                                </div>                                
                        </form>     
    </div>
  </div>
</div>
<button id="btnContacto" class="pegado btn btn-success btn-block">Enviar mensaje al administrador!</button>
<div class="notify">
  <?php if (isset($msgConfirm)) { ?>
        <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-info-circle"></i><?php echo $msgConfirm; ?>
                </div>        
  <?php } ?>
</div>




    

</body>



<script type="text/javascript">

 /*  
$(function () {




function cb(start, end) {

$('#reportrange').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY')).val();
 
 }

    var start = moment().subtract(29, 'days');
var end = moment();
    $('#reportrange').daterangepicker({

        "locale": {
            "format": "YYYY-MM-DD",
            "separator": " -- ",
            "applyLabel": "Guardar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        "startDate": start,
        "endDate": end,
       ranges: {
           'Hoy': [moment(), moment()],
           'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
           'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
           'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
           'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
}
    },cb);
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                $('input#start').val(picker.startDate.format('MM/DD/YYYY'));
                $('input#end').val(picker.endDate.format('MM/DD/YYYY'));
            });


cb(start,end);
});
 */

</script>



<script type="text/javascript" charset="utf-8">

      $(document).ready(function(){
        //para pantalla configuracion
  

        $('#show-up').click(function() {      
          if ($('#p').prop('type')=='password') {
            $('#p').prop('type','text');
          }else {
            $('#p').prop('type','password');
          }        
        });

        $('#show-pe').click(function() {      
              if ($('#pe').prop('type')=='password') {
                $('#pe').prop('type','text');
              }else {
                $('#pe').prop('type','password');
              }        
            });

        //alert('iniciado');
        $('#verify').click(function(){
          user=$('#u').val(); 
          pass=$('#p').val(); 
          host=$('#h').val(); 
          name=$('#n').val();     
          valDb(host, user, pass, name);
        });

        $('#verifyMail').click(function(){      
          email=$('#e').val(); 
          clave=$('#pe').val(); 
          server=$('#s').val(); 
          port=$('#po').val();      
          protocol=$('#pr').val();
          valMail(email, clave, server, port, protocol);
        }); 



        $( "#formContacto" ).hide();
        $("#loader-wiew").css('display', 'none');
        $("#data-view").css('display', 'block');
        $.fn.dataTable.moment = function ( format, locale ) {
            var types = $.fn.dataTable.ext.type;
         
            // Add type detection
            types.detect.unshift( function ( d ) {
                return moment( d, format, locale, true ).isValid() ?
                    'moment-'+format :
                    null;
            } );
         
            // Add sorting method - use an integer for the sorting
            types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
                return moment( d, format, locale, true ).unix();
            };
        };

        $.fn.dataTable.moment('DD-MM-YYYY', 'en');
        //cargando las entradas para la busqueda
        $('#tabla tfoot th').each( function () {
          var title = $('#tabla thead th').eq( $(this).index() ).text();
          if (title=='RUC') {
            $(this).html( '<input size="10" type="text" placeholder="Buscar" />' );
          }
          if (title=='FECHA') {
                  $(this).html( '<input size="10"  class="fechaField" type="text" placeholder="Buscar " />' );
          }
          if (title=='TIPO') {
                  $(this).html( '<input size="10" type="text" placeholder="Buscar " />' );
          }
          if (title=='CLIENTE') {
                  $(this).html( '<input size="30" type="text" placeholder="Buscar " />' );
          }             
        } );

        //inicializando la tabla
        var tabla= $('#tabla').DataTable({
            "columnDefs": [
                         {
                             "targets": [ 5 ],
                             "visible": false,                           
                         }                        
           ],
          language: {
          processing:     "Procesando...",
          search:         "Buscar",
          lengthMenu:     "Mostrar _MENU_ registros",
          info:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          infoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
          infoFiltered:   "(filtrado de un total de _MAX_ registros)",
          infoPostFix:    "",
          loadingRecords: "Cargando...",
          <?php if(isset($hoy)&& isset($mesAnterior)){ ?>
            zeroRecords:    "No hay coincidencias en: (<?php echo $mesAnterior.' <---> '. $hoy; ?>) intente cambiando de rango",
          emptyTable:     "<tr><td colspan='9' heigth='100%'>No hay datos en el rango: (<?php echo $mesAnterior.' <---> '. $hoy; ?>)<td></tr>",
          <?php } ?>
          paginate: {
              first:      "Primero",
              previous:   "Anterior",
              next:       "Siguiente",
              last:       "Ultimo"
          },
          aria: {
              sortAscending:  ": Activar para ordenar la columna de manera ascendente",
              sortDescending: ": Activar para ordenar la columna de manera descendente"
          }
          },                  
          responsive:true,          
        });
  
        $('.fechaField').datepicker({
              changeMonth:true,
              changeYear:true,
              dateFormat:"dd-mm-yy",
              dayNames:["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
              dayNamesShort:["Dom","Lun","Mar","Mié","Jue","Vie","Sáb"],
              dayNamesMin:["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
              monthNames:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
              monthNamesShort:["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"] });
        //$('.actualDate').datepicker('setDate', new Date());

        //Haciendo la busqueda
        tabla.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that.search( this.value ).draw();
            }
        });
      });
      
      var tblAdmin=$('#tbl-admin');
      //Gestionando administradores
      $('#tbl-admin tbody tr').click(function(ev) {
          if ( $(this).hasClass('rowsel') ) {
              $(this).removeClass('rowsel');
             $('#txt-ruc').val("");
              $('#txt-nom').val("");
              $('#txt-email').val("");
              $('#txt-clave').val("");
              $('#txt-rol').val("");
              $('#txt-activo').prop("checked", false);
              $('#txt-accion').val("create");
          }else{          
            tblAdmin.find('.rowsel').removeClass('rowsel');
            $(this).addClass('rowsel');
            $('#paramId').val($(this).prop('id'));
            var fila= new Array(), acum=0;
            $(this).children('td').each(function(i, v){
              fila[acum]=$(this).text();
              acum++;              
            }); 
            $('#txt-ruc').val(fila[0]);
            $('#txt-nom').val(fila[1]);
            $('#txt-email').val(fila[2]);
            $('#txt-clave').val(fila[3]);
            $('#txt-rol').val(fila[4]);
            if (fila[5]==1) {
              $('#txt-activo').prop("checked", true);
            }else{
              $('#txt-activo').prop("checked", false);
            }
            $('#txt-accion').val("update");       
            
          }
      });

      });

    //Boton para usar formulario de contacto
    $( "#btnContacto" ).click(function() {
    $( "#formContacto" ).toggle( "slide", 1000 );

    });
    $( "#btnCancelar" ).click(function() {
      $( "#formContacto" ).toggle( "slide", 1000 );
    });

    function valDb (host, user, pass, name ){
      $.ajax({
          method: "POST",
          url: "validate.php",
          data: { h: host, u: user, p: pass, n:name , opcion: "bdd" }
        })
        .done(function( msg ) {
            alert( msg );
        });
    }
    function valMail (email, pass, server, port, pr ){
      $.ajax({
          method: "POST",
          url: "validate.php",
          data: { e: email, pe: pass, s: server, po:port , opcion: "mail", pro:pr }
        })
        .done(function( msg ) {
            alert( msg );
        });
    }



      
</script>

</html>