<?php 
session_start();  
  include_once("consultas.php");
  require_once('correos.php');  
  if (!isset($_SESSION['loggedin'])) {   
    header('location: login.php');
  }else{
  	require_once 'header.php';  		
	if ($_POST) {		
		$myfile = fopen("config.php", "w") or die("Unable to open file!");
		$openTag="<?php \n";
		$site = "define('SITENAME','".$_POST['sitio']."');\n";
		$ambiente="define('AMBIENTE', '".$_POST['ambiente']."');\n";
		$empresa="define('EMPRESA', '".$_POST['empresa']."');\n";
		$ruc="define('RUC', '".$_POST['ruc']."');\n";
		$from="define('FROM','".$_POST['emailAdmin']."');\n";
		$username="define('USERNAME','".$_POST['emailAdmin']."');\n";
		$password="define('PASSWORD','".$_POST['claveEmail']."');\n";
		$host="define('HOST','".$_POST['host']."');\n";
		$port="define('PORT','".$_POST['port']."');\n";
		$dname="define('DBNAME','".$_POST['dbname']."');\n";
		$dhost="define('DBHOST','".$_POST['dbhost']."');\n";
		$duser="define('DBUSER','".$_POST['dbuser']."');\n";
		$dpass="define('DBPASS','".$_POST['dbpass']."');\n";
		$dprot="define('PROT','".$_POST['protocol']."');\n";
		$closeTag="?>\n";	
		fwrite($myfile, $openTag);
		fwrite($myfile, $site);
		fwrite($myfile, $ambiente);
		fwrite($myfile, $empresa);
		fwrite($myfile, $ruc);
		fwrite($myfile, $from);
		fwrite($myfile, $username);
		fwrite($myfile, $password);
		fwrite($myfile, $host);
		fwrite($myfile, $port);
		fwrite($myfile, $dname);
		fwrite($myfile, $dhost);
		fwrite($myfile, $duser);
		fwrite($myfile, $dpass);
		fwrite($myfile, $dprot);
		fwrite($myfile, $closeTag);
		fclose($myfile);		
	}
	require_once 'config.php';
?>
 <div class="container">
 	<div class="row">
            <div class="col-lg-8">
                <h1>Configuración de Cuenta</h1>
                <p class="lead">
                    Bienvenido: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['username'];
                      echo "(".$_SESSION['role'].")";
                     ?>
                  </a>
                </p>
                <p >
                    Ruc: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['ruc'];         

                     ?>
                  </a>
                <p >
                    E-mail: <a href="acount.php"><span class="glyphicon glyphicon-envelope"></span>
                      <?php echo $_SESSION['email']; ?>
                    </a>
                </p>
                <hr>             

            </div>
  
            <div class="col-md-4">
                  <div class="well">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">                    
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="img/vida.software.png" width ="100%" heigth="100%" alt="Chania">
                        </div>

                        <div class="item">
                          <img src="img/banner.jpg" width ="100%" heigth="100%"  alt="Chania">
                        </div>
                        <div class="item">
                          <img src="img/sample1.png" width ="100%" heigth="100%" alt="Flower">
                        </div>
                      </div>
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                </div>                
            </div>
        </div>


        <!--Real Content-->

        <div class="alert alert-success">
		
		<div class="well">
				<center><h3>Configuración VFPs eDoc</h3></center>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<form class="form" action="" method="POST">
				
				<legend>Datos Empresa</legend>
				<div class="form-group">
					<label>Razón Social:</label>
					<input type="text" name="empresa" class="form-control" required placeholder="Nombre de la empresa o emisor de los documentos electrónicos" value="<?php echo EMPRESA; ?>">
				</div>
				<div class="form-group">
					<label>RUC:</label>
					<input type="text" name="ruc" class="form-control" required placeholder="RUC de la empresa" value="<?php echo RUC; ?>">
				</div>
				<legend>Base de datos</legend>								
					<div class="form-group">
						<label>Base de datos:</label>
						<input id="n" type="text" name="dbname" class="form-control" required placeholder="Nombre de la base de datos" value="<?php echo DBNAME; ?>">
					</div>
					<div class="form-group">
						<label>Servidor base de datos:</label>
						<input id="h" type="text" name="dbhost" class="form-control" required placeholder="localhost, dominio o IP del servidor" value="<?php echo DBHOST; ?>">
					</div>
					<div class="form-group">
						<label>Usuario base de datos:</label>
					<input type="text" id="u" name="dbuser" class="form-control" required placeholder="Nombre del usuario de la base de datos" value="<?php echo DBUSER; ?>">
					</div>
					<div class="form-group">
						<label>Contraseña de usuario:</label>
					<div class="input-group">				      
				     <input type="password" id="p" name="dbpass" class="form-control txt-pass" placeholder="Contraseña del usuario de base de datos" value="<?php echo DBPASS; ?>">
				     <span class="input-group-btn">
				        <button id="show-up" class="btn btn-default" type="button">
				        	<i class="glyphicon glyphicon-eye-open"></i>.</button>
				      </span>
				    </div><!-- /input-group -->				
					</div>
					<div class="form-group">
					<button id="verify" type="button" class="btn btn-info">Verificar conexión</button>
					
					</div>

			</div>
			<div class="col-lg-6">
					<legend>Información general</legend>
					<div class="form-group">
						<label>Nombre del sitio web o aplicación:</label>
						<input type="text" name="sitio" class="form-control" required placeholder="Nombre de la carpeta contenedora de la aplicación web" value="<?php echo SITENAME; ?>">
					</div>					
					<div class="form-group">
						<label>Ambiente:</label>
					<input type="text" id="amb" name="ambiente" class="form-control" required placeholder="pruebas" value="<?php echo AMBIENTE; ?>">
					</div>
					<div class="form-group">
						<label>Email del administrador:</label>
					<input type="email" id="e" name="emailAdmin" class="form-control" required placeholder="admin.facturacion@misitioweb.com" value="<?php echo FROM; ?>">
					</div>
					<div class="form-group">
						<label>Contraseña del email:</label>
					<div class="input-group">				      
				     <input type="password" id="pe" name="claveEmail" class="form-control" required placeholder="Contraseña de la cuenta de correo electrónico" value="<?php echo PASSWORD; ?>">
				     <span class="input-group-btn">
				        <button id="show-pe" class="btn btn-default" type="button">
				        <i class="glyphicon glyphicon-eye-open"></i>.</button>
				      </span>
				    </div><!-- /input-group -->	
					
					</div>
					<div class="form-group">
						<label>Servidor SMTP:</label>
					<input type="text" id="s" name="host" class="form-control" required  placeholder="smtp.misitioweb.com" value="<?php echo HOST; ?>">
					</div>
					<div class="form-group">
						<label>Puerto:</label>
					<input type="text" id="po" name="port" class="form-control" required placeholder="Puerto del servidor SMTP (587)" value="<?php echo PORT; ?>">
					</div>
					<div class="form-group">
						<label>Tipo de conexión cifrada:</label>
					<select id="pr" name="protocol" class="form-control" required>
						<option value="tls">TLS</option>
						<option value="ssl">SSL</option>
					</select>					
					</div>
					<div class="form-group">
					<button id="verifyMail" type="button" class="btn btn-info">Verificar conexión</button>
					
					</div>
					
			</div>			
		</div> 
		<div class="row">
			<div class="form-group">
				<input type="submit" value="Actualizar" class="btn btn-primary btn-block">
			</div>				
		</div>

		</form>
		
	</div>

	<!--Footer seccion-->
		<br>
        <br>
        <hr>
        <div class="footer">
        <div class="row">
          <div class="col-lg-4"></div>
          <div class="col-lg-4">
            
            <center><i class="fa fa-map-marker footer-contacts-icon"></i>
                    Copyright &copy; VIDA SOFTWARE </center>
                    <center>2016-<?php echo date("Y") ?></center>
                    <center>Lima-Perú</center>
                    <center>Factura 2.0</center>
            
          </div>
          <div class="col-lg-4"></div>
          
        </div>
                    
      </div>
 </div>

<?php include_once 'footer.php';
} ?>
