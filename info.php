<?php 

  session_start();  
  include_once("consultas.php");  
  if (!isset($_SESSION['loggedin'])) {
    header('location: login.php');
  }
  else{
    require_once 'header.php';
    include 'user.php';
 ?>    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">
                <h1>Documentos Electrónicos</h1>

                <!-- Author -->
                <p class="lead">
                    Bienvenido: <a href="#"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['username'];
                      echo "(".$_SESSION['role'].")";

                     ?>
                  </a>
                </p>
                <p >
                    Ruc: <a href="#"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['ruc'];         

                     ?>
                  </a>
                </p>

                <?php 
                  if (User::checkPass($_SESSION['ruc'])) {
                    # code...
                  
                 ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Alerta:</strong> Su clave es igual al usuario se recomienda modificarla
                        </div>
                    </div>
                </div>
                <?php } ?>

                <?php 
                  if ( isset($_SESSION['msg']) && $_SESSION['msg']!="") {
                    # code...
                  
                 ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong><?php  echo $_SESSION['msg']; ?></strong> 
                        </div>
                    </div>
                </div>
                <?php } $_SESSION['msg']=""; ?>
                     

            </div>
            <div class="col-md-4">

              
                  <div class="well"> 

                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                   
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="img/vida.software.png" width ="100%" heigth="100%" alt="Chania">
                        </div>

                        <div class="item">
                          <img src="img/banner.jpg" width ="100%" heigth="100%"  alt="Chania">
                        </div>

                        <div class="item">
                          <img src="img/sample1.png" width ="100%" heigth="100%" alt="Flower">
                        </div>

                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>                   
                </div> 
            </div>

        </div>
       
        <hr>
        <div class="row">
          <div class="col-lg-12">
                  <div class="well" id="content">
                    <h1>Información</h1>
                    
                    <object type="application/pdf" data="manual_fact.pdf" width="100%" height="500px"></object>
                  </div>
          </div>
        </div>

        <!-- Footer -->
        <div class="footer">
        <div class="row">
          <div class="col-lg-4"></div>
          <div class="col-lg-4">
            
            <center><i class="fa fa-map-marker footer-contacts-icon"></i>
                    Copyright &copy; VIDA SOFTWARE </center>
                    <center>2016-<?php echo date("Y") ?></center>
                    <center>Lima-Perú</center>
                    <center>Factura 2.0</center>
            
          </div>
          <div class="col-lg-4"></div>
          
        </div>
                    
      </div>

    </div>
  
<?php include_once 'footer.php';
} ?>