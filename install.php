<?php
if (file_exists('config.php')) {
	header('Location: login.php');
}
	if ($_POST) {		
		$myfile = fopen("config.php", "w") or die("Unable to open file!");
		$openTag="<?php \n";
		$site = "define('SITENAME','".$_POST['sitio']."');\n";
		$ambiente="define('AMBIENTE', '".$_POST['ambiente']."');\n";
		$empresa="define('EMPRESA', '".$_POST['empresa']."');\n";
		$ruc="define('RUC', '".$_POST['ruc']."');\n";
		$from="define('FROM','".$_POST['emailAdmin']."');\n";
		$username="define('USERNAME','".$_POST['emailAdmin']."');\n";
		$password="define('PASSWORD','".$_POST['claveEmail']."');\n";
		$host="define('HOST','".$_POST['host']."');\n";
		$port="define('PORT','".$_POST['port']."');\n";
		$dname="define('DBNAME','".$_POST['dbname']."');\n";
		$dhost="define('DBHOST','".$_POST['dbhost']."');\n";
		$duser="define('DBUSER','".$_POST['dbuser']."');\n";
		$dpass="define('DBPASS','".$_POST['dbpass']."');\n";
		$dprot="define('PROT','".$_POST['protocol']."');\n";
		$closeTag="?>\n";	
		fwrite($myfile, $openTag);
		fwrite($myfile, $site);
		fwrite($myfile, $ambiente);
		fwrite($myfile, $empresa);
		fwrite($myfile, $ruc);
		fwrite($myfile, $from);
		fwrite($myfile, $username);
		fwrite($myfile, $password);
		fwrite($myfile, $host);
		fwrite($myfile, $port);
		fwrite($myfile, $dname);
		fwrite($myfile, $dhost);
		fwrite($myfile, $duser);
		fwrite($myfile, $dpass);
		fwrite($myfile, $dprot);
		fwrite($myfile, $closeTag);
		fclose($myfile);
		header('Location: login.php');
	}
	
?>

<html>
<head>
	<title>Instalación VFPs eDoc</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="fa/css/font-awesome.css">

	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>		
</head>
<body>
	<div class="container alert-success">
		<div class="row">
			<div class="well">
					<center><h2>Instalacion VFPs eDoc</h2></center>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<form class="form" action="install.php" method="POST">
				<legend>Datos Empresa</legend>
				<div class="form-group">
					<label>Razón Social:</label>
					<input type="text" name="empresa" class="form-control" required placeholder="Nombre de la empresa o emisor de los documentos electrónicos">
				</div>
				<div class="form-group">
					<label>RUC:</label>
					<input type="text" name="ruc" class="form-control" required placeholder="RUC de la empresa">
				</div>
				<legend>Base de datos</legend>				
					<div class="form-group">
						<label>Base de datos:</label>
						<input id="n" type="text" name="dbname" class="form-control" required placeholder="Nombre de la base de datos">
					</div>
					<div class="form-group">
						<label>Servidor base de datos:</label>
						<input id="h" type="text" name="dbhost" class="form-control" required placeholder="localhost, dominio o IP del servidor">
					</div>
					<div class="form-group">
						<label>Usuario base de datos:</label>
					<input type="text" id="u" name="dbuser" class="form-control" required placeholder="Nombre del usuario de la base de datos">
					</div>
					<div class="form-group">
						<label>Contraseña de usuario:</label>
					<div class="input-group">				      
				     <input type="password" id="p" name="dbpass" class="form-control txt-pass" placeholder="Contraseña del usuario de base de datos">
				     <span class="input-group-btn">
				        <button id="show-up" class="btn btn-default" type="button">
				        	<i class="glyphicon glyphicon-eye-open"></i>.</button>
				      </span>
				    </div><!-- /input-group -->				
					</div>
					<div class="form-group">
					<button id="verify" type="button" class="btn btn-info">Verificar conexión</button>
					
					</div>
			</div>
			<div class="col-lg-6">
					<legend>Información general</legend>
					<div class="form-group">
						<label>Nombre del sitio web o aplicación:</label>
						<input type="text" name="sitio" class="form-control" required placeholder="Nombre de la carpeta contenedora de la aplicación web">
					</div>
					<div class="form-group">
						<label>Ambiente:</label>
						<input type="text" id="amb" name="ambiente" class="form-control" required placeholder="pruebas" >
					</div>
					<div class="form-group">
						<label>Email del administrador:</label>
					<input type="email" id="e" name="emailAdmin" class="form-control" required placeholder="admin.facturacion@misitioweb.com">
					</div>
					<div class="form-group">
						<label>Contraseña del email:</label>
					<div class="input-group">				      
				     <input type="password" id="pe" name="claveEmail" class="form-control" required placeholder="Contraseña de la cuenta de correo electrónico">
				     <span class="input-group-btn">
				        <button id="show-pe" class="btn btn-default" type="button">
				        <i class="glyphicon glyphicon-eye-open"></i>.</button>
				      </span>
				    </div><!-- /input-group -->	
					
					</div>
					<div class="form-group">
						<label>Servidor SMTP:</label>
					<input type="text" id="s" name="host" class="form-control" required  placeholder="smtp.misitioweb.com">
					</div>
					<div class="form-group">
						<label>Puerto:</label>
					<input type="text" id="po" name="port" class="form-control" required placeholder="Puerto del servidor SMTP (587)">
					</div>
					<div class="form-group">
						<label>Tipo de conexión cifrada:</label>
					<select id="pr" name="protocol" class="form-control" required>
						<option value="tls">TLS</option>
						<option value="ssl">SSL</option>
					</select>					
					</div>
					<div class="form-group">
					<button id="verifyMail" type="button" class="btn btn-info">Verificar conexión</button>
					
					</div>
					
			</div>			
		</div> 
		<div class="row">
			<div class="form-group">
				<input type="submit" value="Instalar" class="btn btn-primary btn-block">
			</div>				
		</div>

		</form>
		
	</div>	

</body>
<script type="text/javascript">
	function valDb (host, user, pass, name ){
		$.ajax({
			  method: "POST",
			  url: "validate.php",
			  data: { h: host, u: user, p: pass, n:name , opcion: "bdd" }
			})
			.done(function( msg ) {
			    alert( msg );
			});
	}
	function valMail (email, pass, server, port, pr ){
		$.ajax({
			  method: "POST",
			  url: "validate.php",
			  data: { e: email, pe: pass, s: server, po:port , opcion: "mail", pro:pr }
			})
			.done(function( msg ) {
			    alert( msg );
			});
	}
	$(document).ready(function(){
		$('#show-up').click(function() {			
	        if ($('#p').prop('type')=='password') {
	          $('#p').prop('type','text');
	        }else {
	          $('#p').prop('type','password');
	        }        
      	});

		$('#show-pe').click(function() {			
	        if ($('#pe').prop('type')=='password') {
	          $('#pe').prop('type','text');
	        }else {
	          $('#pe').prop('type','password');
	        }        
      	});

		//alert('iniciado');
		$('#verify').click(function(){
			user=$('#u').val(); 
			pass=$('#p').val(); 
			host=$('#h').val(); 
			name=$('#n').val();			
			valDb(host, user, pass, name);
		});

		$('#verifyMail').click(function(){			
			email=$('#e').val(); 
			clave=$('#pe').val(); 
			server=$('#s').val(); 
			port=$('#po').val();			
			protocol=$('#pr').val();
			valMail(email, clave, server, port, protocol);
		});		

	});

</script>
</html>
<?php 
if (file_exists('config.php')){
	header('Location: login.php');
} 

 ?>