
<?php 
require_once('User.php');
require_once('Tipo.php');
require_once('Model.php');
class Documento extends Model{

	var $unico;
	var $tipo;
	var $fecha;
	var $estab;
	var $ptoemi;
	var $total;
	var $secuencial;
	var $claveac;
	var $numaut;
	var $descargado;
	var $revisado;
	var $enviado;
	var $usrfnx;
	var $fvisual;
	var $user;//TypeUser
	function __construct($usr, $unq, $tipo, $fech, $estab, $ptoemi, $tot, $sec, $claveac, $numaut, $desca, $rev, $env, $usrfnx, $fvisual ){
		parent::__construct();
		$this->user=$usr;//Type User
		$this->unico=$unq;
		$this->tipo=$tipo;		
		$this->fecha=$fech;
		$this->estab= $estab;
		$this->ptoemi= $ptoemi;
		$this->total= $tot;
		$this->secuencial= $sec;
		$this->claveac= $claveac;
		$this->numaut= $numaut;
		$this->descargado= $desca;
		$this->revisado= $rev;
		$this->enviado= $env;
		$this->usrfnx= $usrfnx;
		$this->fvisual= $fvisual;
	}
	static function getMulti($fecha1, $fecha2, $num=9999, $ruc=""){
		$fecha1=date('Y-m-d',strtotime( $fecha1 ));
		$fecha2=date('Y-m-d',strtotime( $fecha2 ));		
		if ($ruc!="") {
			$ruc="AND documento.RUC=".$ruc;
		}

		$query="SELECT 
				documento.RUC,
				documento.COD,
				FECHA,
				UNICO,				
				TOTAL,
				PTOEMI,
				SECUENCIAL,
				ESTAB,
				CLAVEAC,
				NUMAUT,
				DESCARGADO,
				REVISADO,				
				ENVIADO				  
			FROM documento,
				usuario,
				tipodoc 
			WHERE 
				documento.COD=tipodoc.COD AND  
				usuario.RUC=documento.RUC AND FECHA >= DATE('$fecha1') AND FECHA <= DATE('$fecha2') $ruc
			ORDER BY
				FECHA desc LIMIT $num
			
			";
		//echo $query;
		$result = parent::execQuery($query);
		//var_dump($result);
		$lista = array();

		while ($object = $result->fetch_object()) {
			$usuario= User::findByRuc($object->RUC);
			$tipo= Tipo::findById($object->COD);
			$documento = new Documento($usuario, $object->UNICO, $tipo, $object->FECHA, $object->ESTAB, $object->PTOEMI, $object->TOTAL, $object->SECUENCIAL, $object->CLAVEAC, $object->NUMAUT, $object->DESCARGADO, $object->REVISADO,  $object->ENVIADO, null, null);
			array_push($lista, $documento);
		}		
		return $lista;
	}

	static function getByRuc($fecha1, $fecha2, $num=99999){
		$fecha1=date('Y-m-d',strtotime( $fecha1 ));
		$fecha2=date('Y-m-d',strtotime( $fecha2 ));		
		$query="SELECT 
				documento.RUC,
				documento.COD,
				FECHA,
				UNICO,				
				TOTAL,
				PTOEMI,
				SECUENCIAL,
				ESTAB,
				CLAVEAC,
				NUMAUT,
				DESCARGADO,
				REVISADO,				
				ENVIADO				  
			FROM documento,
				usuario,
				tipodoc 
			WHERE 
				documento.COD=tipodoc.COD AND 
				usuario.RUC=documento.RUC AND FECHA >= DATE('$fecha1') AND FECHA <= DATE('$fecha2')
			ORDER BY
				FECHA desc LIMIT $num
			
			";
		$result = parent::execQuery($query);

		$lista = array();

		while ($object = $result->fetch_object()) {
			$usuario= User::findByRuc($object->RUC);
			$tipo= Tipo::findById($object->COD);
			$documento = new Documento($usuario, $object->UNICO, $tipo, $object->FECHA, $object->ESTAB, $object->PTOEMI, $object->TOTAL, $object->SECUENCIAL, $object->CLAVEAC, $object->NUMAUT, $object->DESCARGADO, $object->REVISADO,  $object->ENVIADO, null, null);
			array_push($lista, $documento);
		}
		return $lista;		
	}

}
 ?>