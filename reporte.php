<style type="text/css">
table.display tr th.fe{width: 10%;}


div.panelMain div.panelForm form.form  div.panelForm2  div.fe{

}
div.panelMain div.panelForm form.form  div.panelForm2  div.se{
 
}
div.panelMain div.panelForm form.form  div.panelForm2  div.td{  
  
}
</style>


<?php 
   
  @$td=$_GET['td'];
  @$serie=$_GET['serie'];
  
  session_start(); 
  include_once("consultas.php");
  require_once('correos.php');  
  if (!isset($_SESSION['loggedin'])) {   
    header('location: login.php');
    }
  else {
    include_once 'user.php';
    date_default_timezone_set('America/Lima');
    if (isset($_GET['consultar'])) {
     
     
      $rangoDeFecha  = $_GET['reportrange'] ;      
      @$desde= substr($rangoDeFecha, 0,10);
      @$hasta=substr($rangoDeFecha, 14,10);


    }else{
      
    }
    include_once 'header.php';
 
 ?>


    <!-- Page Content -->
    <div class="container">           
      <div class="row" style="width:102%;">
        <div class="col-lg-6">
          
          <legend>Consulta de Documentos Electrónicos </legend>

          <p class="lead">
        

       <div name ="panelMain" class ="panel panel-info" style="width:208%" >            
            <div class="panel-heading">
              <p style="color: black;">Consulta</p>
            </div>
            <div class="panel-body" name ="panelForm" style="width:105%">
            <form action="reporte.php" method="GET" class="form-inline" name="form" style="width:100%;">
          
         <div class="form-group" name="panelForm2"  style="width:60%;">
               
  
                <div  name ="fe" style="width:50%; float:left;"><input id="reportrange" name= "reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; 
                  border: 1px solid #ccc; width: 60%"  >

    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> 
   </div>

    <div  class="se" name="se">
                <input type="text" name="serie" id="serie" size="8" placeholder="Ingrese serie de 4 digitos" style="border-radius:5px; text-transform: uppercase;width:35%;" maxlength='4'  javascript="string.toUpperCase()" >
              </div>
<div ><input class="btn btn-info" type="submit" value="Consultar" name="consultar" ></div>
              
              
               <div  class="td" name="td"> 
                <select name="td" id="td" style="border-radius:5px">
<option value="01" >Factura</option>
<option value="03">Boleta</option>
<option value="07">Nota de Credito</option>
<option value="08">Nota de Debito</option>
                </select> </div> 
               
              
           <script type="text/javascript">    
   
$(function () {

function cb(start, end) {

$('#reportrange').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY')).val();
 
 }

    var start = moment().subtract(29, 'days');
var end = moment();
    $('#reportrange').daterangepicker({

        "locale": {
            "format": "YYYY-MM-DD",
            "separator": " -- ",
            "applyLabel": "Guardar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizar",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        "startDate": start,
        "endDate": end,
       ranges: {
           'Hoy': [moment(), moment()],
           'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
           'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
           'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
           'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
}
    },cb);
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                $('input#start').val(picker.startDate.format('MM/DD/YYYY'));
                $('input#end').val(picker.endDate.format('MM/DD/YYYY'));
            });


cb(start,end);
});

</script>
        
            </form>
            </div>
          </div></center>

        </div>
     
      </div>

      <legend>Resultado:</legend>      

      <?php 
     
      require_once('document2.php');     
    
      

 
    
        if (isset($_GET['consultar'])){          
          $record = Document::getDatos( "$serie", "$td","$desde","$hasta",0);
            //date('Y-m-d', strtotime($hoy)),0,$_SESSION['ruc']);                 
       }else{          
          $record = Document::getDatos( "$serie","$td" , "$desde","$hasta", 20);
          //date('Y-m-d', strtotime($hoy)), 20, $_SESSION['ruc']);
        }
       
       ?>


      <!--Data table-->
      <div class="row text-center" id="loader-wiew">          
          <img id="img-loading" class="img-responsive" src="img/loader.gif">          
          <p>Procesando por favor espere....</p>
      </div>
      <div id="data-view">
      <table id="tabla" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                
                <?php if ($_SESSION['role']=='admin') {?>
                 
                <?php }  ?>
                
                <th class="fe">Fecha</th>
                <th>Tipo Documento</th>
                <th>SERIE</th>
                <th>NUMERO</th>
                <th>Total Venta</th>                
                <th>Tipo Moneda</th>
                <th>Documento Cliente</th>
                <th>Estado de Documento</th>
                <th>Mensaje de Error</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                
                <?php if ($_SESSION['role']=='admin') {?>
                 
                <?php } ?>
                <th class="fe">Fecha</th>
                <th>Tipo Documento</th>
                <th>SERIE</th>
                <th>NUMERO</th>
                <th>Total Venta</th>              
                <th>Tipo Moneda</th>
                <th>Documento Cliente</th>
                <th>Estado deDocumento</th>
                <th>Mensaje de Error</th>
            </tr>
        </tfoot>
        <tbody>   
           <?php 
           /*$conexionn=mysqli_connect("localhost", "root","","corpora");
           $sql= "SELECT * FROM oficina_fecha where  serie='$serie' ";
           $query=mysqli_query($conexionn,$sql);*/
          // while($doc= mysqli_fetch_array($query)) { 
 while($doc= mysqli_fetch_array($record)) {
           ?>
              <?php if ($_SESSION['role']=='admin') {?>
                
                <td><?php echo $doc['documentocliente']; ?></td>
                <td><?php echo htmlentities($doc['tipodocumento'], ENT_QUOTES, 'UTF-8'); ?> </td>
                <?php } ?>
<tr>
                <td><?php echo $doc['fechaemision'] ?></td>
                <td><?php echo $doc['tipodocumento'] ?></td>  
                <td><?php echo $doc['serie'] ?></td>  
                <td><?php echo $doc['numero'] ?></td>             
                <td ><?php echo $doc['totalventa']; ?></td>
                <td><?php echo $doc['tipomoneda']?></td>
                <td><?php echo $doc['documentocliente'] ?></td> 
                <td><?php echo $doc['estadoDocumento'] ?></td>
                <td><?php echo $doc['mensajeerror'] ?></td> 
            </tr>

           
           <?php 
            }//End Foreach
            ?>  
        </tbody>       
    </table>
    
  </div>     
     
     <hr>

     <div class="footer">
        <div class="row">
          <div class="col-lg-4"></div>
          <div class="col-lg-4">
            
            <center><i class="fa fa-map-marker footer-contacts-icon"></i>
                    Copyright &copy; VIDA SOFTWARE </center>
                    <center>2016-<?php echo date("Y") ?></center>
                    <center>Lima-Perú</center>
                    <center>Factura 2.0</center>
   
          </div>
          <div class="col-lg-4"></div>
          
        </div>
                    
      </div> 

    </div>

<?php include_once 'footer.php'; 
}

?>