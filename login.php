<?php

    if (!file_exists('config.php')) {
        header('Location: install.php');
    }    

    session_start();
    if (isset($_SESSION['loggedin'])) {
        # code...
    
        if ($_SESSION['loggedin']) {
            # code...
            header('location: index.php');
        } 
    }   
 ?>


<html>
<head><meta http-equiv="Content-Type" content="text/html;">
    <meta charset="utf-8">
    <meta name="description" content="Facturación electrónica SUNAT">
    <meta name="author" content="VFPSTEAM BI SOLUTIONS">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
    <title>Inicio de sesión</title>

       
    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <style type="text/css">
        #head{
            margin-top: 30px;
            border-radius: 30px;
            border-style: solid;
            border-color: #428BCA;
        }

        #log{            
            border-style: solid;
            border-color: #428BCA;
        }

        body {
          background-image: url("img/fondo.jpg"); 
          background-color: #cccccc;
          background-position: -100px -100px; 
          background-attachment:fixed;
        }
        .help{
            position: fixed;
            bottom: 50px;
            right: 60px;
            color: black;
            font-size: 40px;      
        }        

    </style>

</head>
<body >
        
        <div class="container" > 
        <div class="row">
            <div align="center" class="well" id="head"><h1>Bienvenido al Sistema de Comprobantes de Pago Electrónicos</h1></div>
        </div>

        <div class="row">
        

        <div  style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" id="log" >
                    <div class="panel-heading">
                        <div class="panel-title">Inicio de sesión</div>
                        <div style="float:right; position: relative; top:-20px"><a class="btn-danger" href="resetaccount.php"><strong>¿Olvido su contraseña?</strong></a></div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="loginform" class="form-horizontal" role="form" action="auth.php" method="POST">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="login-username" type="text" class="form-control" name="user" value="" placeholder="RUC/DNI">                                        
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" id="p" name="pass" class="form-control txt-pass" placeholder="Contraseña">
                                <span class="input-group-btn">
                                    <button id="show-up" class="btn btn-default" type="button">
                                    <i class="glyphicon glyphicon-eye-open"></i>.</button>
                                </span>
                            </div>
                                    

                                
                            <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-lg-12 controls">
                                      <input type="submit" class="btn btn-block btn-primary" value="Login">
                                      

                                    </div>
                                </div>                                
                        </form>




                        </div>                     
                    </div>  
                 </div>

          </div>         
          
                    
      </div>      
        
    </div>
    <div class="help">
        <a href="help.php">Ayuda<img src="img/help.gif"></a>
    </div>
    

</body>
    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
        $('#show-up').click(function() {

            if ($('#p').prop('type')=='password') {
              $('#p').prop('type','text');
            }else {
              $('#p').prop('type','password');
            }        
        });
    });
    </script>
</html>
