
<?php

  session_start();   
  if (!isset($_SESSION['loggedin'])) {
    echo "Lo sentimos no tienes permisos";
    header('location: login.php');
  }
  else{
    include_once 'header.php';
 ?>

    <div class="container">    
      <div class="row">
          <div class="col-lg-8">
            <h1>Configuración de Cuenta</h1>
            <p class="lead">
                Bienvenido: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                  <?php 
                  echo $_SESSION['username'];
                  echo "(".$_SESSION['role'].")";

                 ?>
              </a>
            </p>
            <p >
                Ruc: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                  <?php echo $_SESSION['ruc']; ?>
              </a>
            <p >
                E-mail: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                  <?php $_SESSION['email'];?>
                </a>
            </p>
            <hr>   
          <div class="well" >              
            <div class="row">
                <div class="panel panel-primary" id="log" >
                  <div class="panel-heading">
                      <div class="panel-title">Cambio de contraseña personal</div>
                      
                  </div>     

                  <div style="padding-top:30px" class="panel-body" >

                      <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                          
                      <form id="loginform" class="form-horizontal" role="form" action="change.php" method="POST">
                                  
                        <div style="margin-bottom: 25px" class="input-group">
                                    
                          <input id="login-username" type="password" class="form-control" name="pass1" value="" placeholder="Nueva contraseña">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>                                                  
                        </div>
                              
                          <div style="margin-bottom: 25px" class="input-group">
                                      
                                      <input id="login-password" type="password" class="form-control" name="pass2" placeholder="Repita su nueva contraseña"><span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                          </div>
                                  

                              
                          <div style="margin-top:10px" class="form-group">
                                  <!-- Button -->

                                  <div class="col-lg-12 controls">
                                    <input type="submit" class="btn btn-block btn-success" value="Enviar">
                                    

                                  </div>
                              </div>                                
                      </form>     



                      </div>                     
                  </div>  
              

        </div>

            </div>
            

          </div>
          <div class="col-md-4">

                <div class="well"> 
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    
                    <div class="carousel-inner" role="listbox">
                      <div class="item active">
                        <img src="img/vida.software.png" width ="100%" heigth="100%" alt="Chania">
                      </div>

                      <div class="item">
                        <img src="img/banner.jpg" width ="100%" heigth="100%"  alt="Chania">
                      </div>

                      <div class="item">
                        <img src="img/sample1.png" width ="100%" heigth="100%" >
                      </div>

                    </div>
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>

              </div>
              
          </div>

      </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <div class="footer">
        <div class="row">
          <div class="col-lg-4"></div>
          <div class="col-lg-4">
            
            <center><i class="fa fa-map-marker footer-contacts-icon"></i>
                    Copyright &copy; VIDA.SOFTWARE </center>
                    <center>2016-<?php echo date("Y") ?></center>
                    <center>Lima-Perú</center>
                    <center>Factura 1.0</center>
            
          </div>
          <div class="col-lg-4"></div>
          
        </div>
                    
      </div>

    </div>

<?php 
include_once 'footer.php';
} ?>