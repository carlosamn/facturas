<?php
/**
 * 
 */
require_once('conectar.php');
require_once('user.php');
require_once('tipo.php');

class Document{
	public $td;
	public $numero;
	public $fechaemision;
	public $totalventa;
	public $subtotal;
	public $totaligv;
	public $tipomoneda;
	public $documentocliente;
	public $serie;
	
	

	
		

	
	function __construct($rs) {
		$this->td = $rs['tipodocumento'];
		$this->serie = $rs['serie'];
		$this->numero = $rs['numero'];
		$this->fechaemision = $rs['fechaemision'];
		$this->totalventa = $rs['totalventa'];
		$this->subtotal = $rs['subtotal'];
		$this->totaligv = $rs['totaligv'];
		$this->tipomotneda = $rs['tipomoneda'];
		$this->documentocliente = $rs['documentocliente'];
		
		
	}






 	public static function getDatos($serie,$td,$desde , $hasta,  $lim=0){
 		(empty($_POST['action'])) ? 'default' : $_POST['action'];

 		$limiter=($lim==0) ? '': "LIMIT $lim";
 		$con = Conector::getConexion();
 		
 		$query="SELECT
		*
		FROM
			oficina_fecha
		WHERE
		serie='$serie' and tipodocumento='$td' and fechaemision BETWEEN '{$desde}' AND '{$hasta}' {$limiter}   ";		
 		$res=mysqli_query($con,$query);
		return $res;
 	}

 	public static function lookup($condicion, $orden, $lim=0){
 		$con = Conector::getConexion();
 		$limiter=($lim==0) ? '': "LIMIT $lim";
		$query="SELECT `td`, `serie`, `numero`, `fechaemision`, `totalventa`, `subtotal`, `totaligv`, `tipomoneda`, `documentocliente`
		FROM oficina_fecha 
		WHERE {$condicion} {$orden} {$limiter};";
		$rs=mysqli_query($con, $query);		
		if (mysqli_num_rows($rs)>0) {
			if(mysqli_num_rows($rs)==1){
				$row= mysqli_fetch_array($rs);
				return new Document($row);
			}else{
				$list = array();
				while ($row = mysqli_fetch_array($rs)) {
					array_push($list, new Document($row));
				}
				return $list;
			}			
		}			
		else{
			return false;
		}
		
 	}

 	public function getUser(){
 		$u = User::lookup('RUC', $this->ruc, 1);
 		if ($u){
 			$row=mysqli_fetch_array($u);
 			$usu= new User($row['NOMBRE'], $row['RUC'], $row['EMAIL'], $row['PASS'], $row['ROLE']);
 			$usu->activo=$row['ACTIVO'];
 			return $usu;
 		}  			
 		else{
 			return false;
 		} 			
 	}
 	public function getTipo(){
 		return Tipo::lookup('COD', $this->codigo, 1);
 	}
 	public function setEnviado(){
 		$con = Conector::getConexion();
 		$query = "UPDATE documento SET enviado=1 WHERE UNICO='{$this->unico}'"; 	
 		mysqli_query($con, $query); 		
 	}

 	public function increaseTries(){
 		$con = Conector::getConexion(); 		
 		$query = "UPDATE documento SET TRIES=COALESCE(TRIES, 0)+1 WHERE UNICO='{$this->unico}'"; 	
 		mysqli_query($con, $query);
 	} 
 	public function putLogEnvio($log){
 		$con = Conector::getConexion(); 		
 		$this->logEnvio=$log;
 		$query = "UPDATE documento SET LOGENVIO='{$log}' WHERE UNICO='{$this->unico}'"; 		
 		mysqli_query($con, $query);
 	}
} 

?>