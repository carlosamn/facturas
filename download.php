<?php
session_start();  
if (!isset($_GET['val']) || empty($_GET['val'])) {
 exit();
}
include_once("conectar.php");
include_once("consultas.php"); 
$unq=$_GET['val'];
$opt=$_GET['ty'];
$dat=$_GET['dat'];
$conex= Conector::getConexion();
$auth=getDoc($conex,$unq);
/*if is the other case uncomment line 14
	and replace the line 20 by :$auth=$pdfnombre.".pdf";
*/
	
if ($opt=="x") {
	# code...
	$auth=$auth.".xml";
	$root = "xml/";
	$root2 = "xml2/" . substr($dat, 0, 4) . "/" . substr($dat, 5, 2) . "/" . substr($dat, 8, 2) . "/";
}else{
	$auth=$auth.".pdf";
	$root = "pdf/";
	$root2 = "pdf2/" . substr($dat, 0, 4) . "/" . substr($dat, 5, 2) . "/" . substr($dat, 8, 2) . "/";
}

$file = basename($auth);
$path = $root.$file;
$type = '';

//if (!is_file($path)) {

//if (!is_file($path)) {
	//LP: 30/05/2017
	//Cuando no existe el pdf, buscarlo en pdf2 como zip
	//$file = basename($auth.".zip");
	$path = $root2.$file;
	if (!is_file($path)){

		//Si no existe buscar zip
		$filezip = basename($auth.".zip");
		$pathzip = $root2.$filezip;
		if (is_file($pathzip)){
			//sis existe zip, desempaquetar
			$zip = new ZipArchive;
			if ($zip->open($pathzip) == TRUE){
				$zip->extractTo($root2);
    			$zip->close();
			}
		}
	}

	//Si no existe en pdf2 o xml2 buscar en pdf o xml
	if (!is_file($path)){
		$path = $root.$file;
	}
//}

//echo("<script type='text/javascript'>console.log('PHP file: ".$file."');");
//echo("console.log('PHP path: ".$path."');</script>");

if (is_file($path)) {
 $size = filesize($path);
 if (function_exists('mime_content_type')) {
 	$type = mime_content_type($path);
 } else if (function_exists('finfo_file')) {
	 $info = finfo_open(FILEINFO_MIME);
	 $type = finfo_file($info, $path);
	 finfo_close($info);
 }
 if ($type == '') {
 	$type = "application/force-download";
 }
 // Definir headers
 header("Content-Type: $type");
 header("Content-Disposition: attachment; filename=$file");
 header("Content-Transfer-Encoding: binary");
 header("Content-Length: " . $size);
 // Descargar archivo
 readfile($path);
 if ($_SESSION['role']=="user"){
 	setDownloaded($conex,$unq);
 }
} else {
 die("El archivo no existe. <a href=\"index.php\">volver</a>");
}
  
?>
