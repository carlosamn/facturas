<?php 
  session_start(); 
      @$rangoDeFecha  = $_GET['reportrange'] ;      
      @$desde= substr($rangoDeFecha, 0,10);
      @$hasta=substr($rangoDeFecha, 16,10);
  include_once("consultas.php");
  require_once('correos.php');  
  if (!isset($_SESSION['loggedin'])) {   
    header('location: login.php');
    }
  else{
    include_once 'user.php';
    date_default_timezone_set('America/Lima');
    if (isset($_GET['consultar'])) {
  
    }else{
      //$hoy=date("d-m-Y");
     /* $now=new DateTime(date('Y-m-d'));
      $hoy= date_format($now,"d-m-Y");

      $monthAgo  = date_sub($now,date_interval_create_from_date_string("1 months"));
      $mesAnterior= date_format($monthAgo,"d-m-Y");*/
    }
    include_once 'header.php';
 ?>


    <!-- Page Content -->
    <div class="container">           
      <div class="row">
        <div class="col-lg-6">
          
          <legend>Consulta de Documentos Electrónicos </legend>

          <p class="lead">
            Bienvenido: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
              <?php echo $_SESSION['username'];  echo "(".$_SESSION['role'].")";//php code  ?>
              </a>
            </p>
            <p >
                Ruc: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                  <?php echo $_SESSION['ruc'];?>
              </a>
            </p>

            <?php if (User::checkPass($_SESSION['ruc'])) { ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  <strong>Alerta:</strong> Su contraseña es igual al usuario se recomienda modificarla <a class='btn btn-primary' href='acount.php'><i class='glyphicon glyphicon-lock'></i>aqui</a>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if ( isset($_SESSION['msg']) && $_SESSION['msg']!="") { ?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-warning alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="fa fa-info-circle"></i>  <strong><?php  echo $_SESSION['msg']; ?></strong> 
                    </div>
                </div>
            </div>
            <?php } $_SESSION['msg']=""; ?>

        <div class="panel panel-info" style="width:78%;">            
            <div class="panel-heading">
              <p style="color: black;">Rango de consulta</p>
            </div>
            <div class="panel-body" >
            <form action="index.php" method="GET" class="form-inline">
              <div class="form-group">
                        <div  name ="fe" style="width:180%; float:left;"><input id="reportrange" name= "reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; 
                  border: 1px solid #ccc; width: 60%"  >

    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> 
   </div>  
              </div>
                         
              <input style="float:right;"class="btn btn-info" type="submit" value="Consultar" name="consultar">
            
                 <script type="text/javascript" src="js/daterangefunction.js">    
   


</script>

            </form>
            </div>
          </div>

        </div>
        <div class="col-lg-6">
          <div class="well hidden-xs" >

        <div id="myCarousel" class="carousel slide" data-ride="carousel">                      
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="img/vida.software.png" >
                        </div>

                        <div class="item">
                          <img src="img/banner.jpg" >
                        </div>

                        <div class="item">
                          <img src="img/sample1.png" >
                        </div>

                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
            </div>


        </div>
                    
        </div>
      </div>

      <legend>Resultados:</legend>      

      <?php 
      require_once('document.php');     

      if ($_SESSION['role']=='admin') {       
        
        if (isset($_GET['consultar'])){        
        $record = Document::getDatos( $desde, 
           $hasta, 0, '');        
        }else{
          $record=Document::getDatos($mesAnterior,
         $hoy, 20, '');
        }
      }else{        
        if (isset($_GET['consultar'])){          
          $record = Document::getDatos( $desde,
             $hasta,0,$_SESSION['ruc']);                 
        }else{          
          $record = Document::getDatos( $desde,
          $hasta, 20, $_SESSION['ruc']);
        }
      }
         
            
      
            
       ?>


      <!--Data table-->
      <div class="row text-center" id="loader-wiew">          
          <img id="img-loading" class="img-responsive" src="img/loader.gif">          
          <p>Procesando por favor espere....</p>
      </div>
      <div id="data-view">
      <table id="tabla" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>XML</th>
                <th>PDF</th>
                <?php if ($_SESSION['role']=='admin') {?>
                  <th>RUC</th>
                  <th>CLIENTE</th>
                <?php } ?>
                
                <th>FECHA</th>
                <th>TIPO</th>
                <th>TOTAL</th>
                <th>REV</th>
                <th>DESC</th>
                <th>NUM_DOC</th>                
                <th>CLAVE ACCESO</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>XML</th>
                <th>PDF</th>
                <?php if ($_SESSION['role']=='admin') {?>
                  <th>RUC</th>
                  <th>CLIENTE</th>
                <?php } ?>
                <th>FECHA</th>
                <th>TIPO</th>
                <th>TOTAL</th>
                <th>REV</th>
                <th>DESC</th>
                <th>NUM_DOC</th>                
                <th>CLAVE ACCESO</th>
            </tr>
        </tfoot>
        <tbody>   
           <?php while($doc= mysqli_fetch_array($record)) { ?>

            <tr>
                <td>
                  <a href="download.php?val=<?php echo $doc['UNICO']?>&ty=x&dat=<?php echo $doc['FECHA']?>"> <img src="img/xmlog.gif" heigth="30" width="30"></a>
                </td>

                <td>
                  <a href="download.php?val=<?php echo $doc['UNICO'] ?>&ty=p&dat=<?php echo $doc['FECHA']?>"><img src="img/pdflog.png" heigth="30" width="30"></a>
                </td>
                 <?php if ($_SESSION['role']=='admin') {?>
                <td><?php echo $doc['RUC']; ?></td>
                <td><?php echo htmlentities($doc['NOMBRE'], ENT_QUOTES, 'UTF-8'); ?> </td>
                <?php } ?>

                <td><?php echo date('d-m-Y',strtotime($doc['FECHA'])) ?></td>

                <td><a href="download.php?val=<?php echo $doc['UNICO'] ?>&ty=p&dat=<?php echo $doc['FECHA']?>">
                  <?php echo $doc['DESCRIPCION'] ?></a></td>             
                <td class="numeric"><?php echo number_format($doc['TOTAL'],2 ,'.',','); ?></td>
                <td><span class="invisible"><?php echo $doc['REVISADO']?></span><?php  if($doc['REVISADO']=='1'){?><i class="glyphicon glyphicon-ok bien"></i> <?php }else{ ?> <i class="glyphicon glyphicon-remove mal"></i> <?php  } ?></td>

                <td><span class="invisible" ><?php echo$doc['DESCARGADO']?></span><?php  if($doc['DESCARGADO']=='1'){?><i class="glyphicon glyphicon-ok bien"></i> <?php }else{ ?> <i class="glyphicon glyphicon-remove mal"></i> <?php  } ?></td>
                <td><?php echo $doc['ESTAB'].'-'.$doc['PTOEMI'].'-'.$doc['SECUENCIAL'] ?></td> 

                <td><?php echo $doc['CLAVEAC'] ?></td>
            </tr>

           
           <?php 
            }//End Foreach
            ?>  
        </tbody>       
    </table>
    
  </div>     
     
     <hr>

     <div class="footer">
        <div class="row">
          <div class="col-lg-4"></div>
          <div class="col-lg-4">
            
            <center><i class="fa fa-map-marker footer-contacts-icon"></i>
                    Copyright &copy; VIDA SOFTWARE </center>
                    <center>2016-<?php echo date("Y") ?></center>
                    <center>Lima-Perú</center>
                    <center>Factura 2.0</center>
   
          </div>
          <div class="col-lg-4"></div>
          
        </div>
                    
      </div> 

    </div>

<?php include_once 'footer.php'; 
}?>