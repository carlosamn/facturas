<?php 

  session_start();
  include_once("conexion.php");
  include_once("consultas.php");  
  if (!isset($_SESSION['loggedin']) || !isset($_GET['fecha']) || !isset($_GET['fechafin'])) {    
    header('location: login.php');
  }
  else{
 ?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Facturación electrónica SUNAT">
  <meta name="author" content="VFPSTEAM BI SOLUTIONS">
  <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<title>Consulta de Documentos Electrónicos</title>

	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">  
  <link href="css/blog-post.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/cupertino/jquery.ui.theme.css">
  <link rel="stylesheet" type="text/css" href="css/cupertino/jquery-ui.css">

  <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>  

  <script >
      $(document).ready(function() {
        $(function(){
         
          $("#date").datepicker(
            {
              changeMonth:true,
              changeYear:true,
              dateFormat:"yy/mm/dd",            
            }
            );
          $("#date").datepicker("setDate", new Date());

          $("#datefin").datepicker(
            {
              changeMonth:true,
              changeYear:true,
              dateFormat:"yy/mm/dd",            
            }
            );
          $("#datefin").datepicker("setDate", new Date());

        })
      });
  </script>

  <style type="text/css">
    body {
      background-image: url("img/fondo.jpg"); 
      background-color: #cccccc;
      background-position: -100px -100px; 
      background-attachment:fixed;
    }

  </style>

  
</head>
<body  >
	


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home"></span>Documentos Electrónicos</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="info.php">Información</a>
                    </li>
                    <li>
                        <a href="acount.php">Cuenta</a>
                    </li>
                    <li>
                        <a href="logout.php">Logout<span class="glyphicon glyphicon-user"></span></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->

                <!-- Title -->
                <h1>Documentos Electrónicos</h1>

                <!-- Author -->
                <p class="lead">
                    Bienvenido: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['username'];
                      echo "(".$_SESSION['role'].")";

                     ?>
                  </a>
                </p>
                <p >
                    Ruc: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['ruc'];         

                     ?>
                  </a>
                </p>

                <?php 
                  if (checkPass($conex,$_SESSION['ruc'])) {
                    # code...
                  
                 ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Alerta:</strong> Su clave es igual al usuario se recomienda modificarla <a href="acount.php">aqui!!</a>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <?php 
                  if ( isset($_SESSION['msg']) && $_SESSION['msg']!="") {
                    # code...
                  
                 ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong><?php  echo $_SESSION['msg']; ?></strong> 
                        </div>
                    </div>
                </div>
                <?php } $_SESSION['msg']=""; ?>




                <hr>              

                <!-- Preview Image
                <img class="img-responsive" src="img/Fondo_de.jpg" heigth="300px" width="900px" alt="">

                <hr> -->

              
       <?php 
       if (isset($_GET['opcion'])) {
          $opcion=$_GET['opcion'];
       }else{
            $opcion=array("","");
       } 
      
      $fecha=$_GET['fecha'];
      $fechafin=$_GET['fechafin'];
      $rucsearch=$_GET['rucsearch'];
                 
       ?>       
      
      <!--<div class="well" id="content">-->
        

      
        <h1>Resultados</h1>
        <div class="table-responsive">
        <?php 
                  
          $result=general($conex,$opcion,$fecha,$fechafin, $_SESSION['role'],$_SESSION['ruc'],$rucsearch);
          if ($_SESSION['role']=="admin") {
            # code...
          
              echo "<table class=\"table table table-bordered table-hover table-striped \">";
              echo "<tr><td colspan=\"2\">DOCS</td><td>RUC</td><td>FECHA</td><td>TIPO</td><td>TOTAL</td><td>NUM-DOC</td><td>REVISADO</td><td>DESCARGA</td><td>CLAVE ACCESO</td>";
              while ($fila=mysqli_fetch_array($result)) {
                  # code...
                  echo "<tr>";
                  echo "<td><a href=\"download.php?val=".$fila['UNICO']."&ty=x\"><img src=\"img/xmlog.gif\" heigth=\"30\" width=\"30\"></a></td><td><a href=\"download.php?val=".$fila['UNICO']."&ty=p&dat=".$fila['COD']."\"><img src=\"img/pdflog.png\" heigth=\"30\" width=\"30\"></a></td>"."<td>".$fila['RUC']."</td>"."<td>".$fila['FECHA']."</td>"."<td><a href=\"viewer.php?doc=".$fila['UNICO']."\">".$fila['DESCRIPCION']."</a></td>"."<td>".$fila['TOTAL']."</td>"."<td>".$fila['ESTAB']."-".$fila['PTOEMI']."-".$fila['SECUENCIAL']."</td>";


                  if($fila['REVISADO']==0){
                    echo "<td align=\"center\" ><img src=\"img/cross.png\" heigth=\"30\" width=\"30\"></td>";
                  }else{
                    echo "<td align=\"center\"><img src=\"img/check.png\" heigth=\"30\" width=\"30\"></td>";
                  }

                  
                  if($fila['DESCARGADO']==0){
                    echo "<td align=\"center\"><img src=\"img/cross.png\" heigth=\"30\" width=\"30\"></td>";
                  }else{
                    echo "<td align=\"center\"><img src=\"img/check.png\" heigth=\"30\" width=\"30\"></td>";
                  }

                  echo "<td>".$fila['CLAVEAC']."</td>";
                  echo "<tr>";
              }
              echo "</table>";
            }else{

              echo "<table class=\"table table table-bordered table-hover table-striped \">";
              echo "<tr><td colspan=\"2\">DOCS</td><td>FECHA</td><td>TIPO</td><td>TOTAL</td><td>NUM-DOC</td><td>REVISADO</td><td>DESCARGA</td><td>CLAVE ACCESO</td>";
              while ($fila=mysqli_fetch_array($result)) {
                  # code...
                  echo "<tr>";
                  echo "<td><a href=\"download.php?val=".$fila['UNICO']."&ty=x\"><img src=\"img/xmlog.gif\" heigth=\"30\" width=\"30\"></a></td><td><a href=\"download.php?val=".$fila['UNICO']."&ty=p&dat=".$fila['COD']."\"><img src=\"img/pdflog.png\" heigth=\"30\" width=\"30\"></a></td>"."<td>".$fila['FECHA']."</td>"."<td><a href=\"viewer.php?doc=".$fila['UNICO']."\">".$fila['DESCRIPCION']."</a></td>"."<td align=\"right\">".$fila['TOTAL']."</td>"."<td>".$fila['ESTAB']."-".$fila['PTOEMI']."-".$fila['SECUENCIAL']."</td>";


                  if($fila['REVISADO']==0){
                    echo "<td align=\"center\"><img src=\"img/cross.png\" heigth=\"30\" width=\"30\"></td>";
                  }else{
                    echo "<td align=\"center\"><img src=\"img/check.png\" heigth=\"30\" width=\"30\"></td>";
                  }


                  if($fila['DESCARGADO']==0){
                    echo "<td align=\"center\"><img src=\"img/cross.png\" heigth=\"30\" width=\"30\"></td>";
                  }else{
                    echo "<td align=\"center\"><img src=\"img/check.png\" heigth=\"30\" width=\"30\"></td>";
                  }

                  echo "<td>".$fila['CLAVEAC']."</td>";
                  echo "<tr>";
              }
              echo "</table>";

            }
         ?>
         </div>


      <!--</div>-->
  
                     

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <?php if ($_SESSION['role']=="admin") {
                  # code...
                ?>                
                <?php  }
                else{ ?>
                  <div class="well"> 

                    <!--For Carrousel-->

                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators 
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        
                      </ol>-->

                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="img/Fondo_de.jpg" >
                        </div>

                        <div class="item">
                          <img src="img/banner.jpg" >
                        </div>

                        <div class="item">
                          <img src="img/sample1.png" >
                        </div>

                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>

                    <!--End For Carrousel-->










                    <!--<img class="img-responsive" src="img/Fondo_de.jpg" heigth="300px" width="900px" alt="">-->
                    <!-- /.input-group -->
                </div>
                 <?php
                } ?>
                <!-- Blog Categories Well -->
                <div class="well">
                     <form action="search.php" method="GET">
                    <h3>Filtro de busqueda</h3>
                    <h5>Ruc(Opcional):</h5>
                    <div class="input-group">
                        
                        <span class="input-group-btn">
                            <button class="btn btn-default">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                        <input type="text" name="rucsearch" class="form-control">


                        
                    </div>
                    
                    <!-- /.input-group -->

                    
                    <h5>Tipo:</h5>
                   
                    <div class="row">
                        <div class="col-lg-6">
                                             

                            <ul class="list-unstyled">
                                <li>
                                  <div class="checkbox">
                                    <label>
                                      <input name="opcion[]" type="checkbox" value="01"><b>Facturas<b>
                                    </label>
                                  </div>
                                </li>  
                                <li>
                                    <div class="checkbox">
                                      <label>
                                          <input name="opcion[]" type="checkbox" value="04"> <b>Nota de Crédito</b> 
                                      </label>
                                    </div>
                                </li>
                                <li>
                                  <div class="checkbox">
                                    <label>
                                      <input name="opcion[]" type="checkbox" value="07"> <b>Retenciónes</b> 
                                    </label>
                                  </div>
                                </li>                                  
                            </ul>
                        </div>
                        <div class="col-lg-6">
                          <ul class="list-unstyled">
                            
                            <li>
                              <div class="checkbox">
                                    <label>
                                      <input name="opcion[]" type="checkbox" value="05"> <b>Nota de Débito</b> 
                                    </label>
                            </li>
                            <li>
                              <div class="checkbox">
                                    <label>
                                      <input name="opcion[]" type="checkbox" value="06"> <b>Guía de Remición</b> 
                                    </label>
                              </div>
                            </li>

                          </ul>
                        </div>
                    </div>
                    <!-- /.row -->
                
                    <h5>Rango de Fecha:</h5>
                    
                      <div class="form-group">
                        <label class=" control-label">Desde:</label>
                        <div class="">
                          <div class="input-group">
                            <span class="input-group-addon">  
                                  <span class="glyphicon glyphicon-calendar"></span>     

                            </span>
                            <input type="text" id="date" name="fecha" required  class="form-control">
                          </div>
                        </div>
                      </div> 
                                            <!-- Prepended text-->
                      <div class="form-group">
                        <label class=" control-label">Hasta:</label>
                        <div class="">
                          <div class="input-group">
                            <span class="input-group-addon">  
                                  <span class="glyphicon glyphicon-calendar"></span>     

                            </span>
                            <input type="text" id="datefin" name="fechafin" required  class="form-control">
                          </div>
                        </div>
                      </div> 
                      <br>
                      <div class="input-group">
                          <span class="input-group-btn">
                              <button class="btn btn-primary btn-block" id="btncal" type="submit" >
                                  <span class="glyphicon glyphicon-search"></span>Buscar
                              </button>
                          
                          </span>
                           <span class="input-group-btn">
                              <a class="btn btn-success btn-block"  href="index.php" >
                                  <span class="glyphicon glyphicon-filter"></span>Limpiar Filtros
                              </a>
                          
                          </span>

                      </div>
                      
                    </form>
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; VIDA SOFTWARE</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>

    

</body>
</html>
<?php } ?>