<?php
/**
 * 
 */
require_once('conectar.php');
require_once('user.php');
require_once('tipo.php');
class Document{
	public $unico;
	public $codigo;
	public $ruc;
	public $fecha;
	public $estab;
	public $ptoemi;
	public $total;
	public $secuencial;
	public $claveac;
	public $numaut;
	public $descar;
	public $revisado;
	public $enviado;
	public $tries;
	public $logEnvio;
	
	
	function __construct($rs) {
		$this->unico = $rs['UNICO'];
		$this->codigo = $rs['COD'];
		$this->ruc = $rs['RUC'];
		$this->fecha = $rs['FECHA'];
		$this->estab = $rs['ESTAB'];
		$this->ptoemi = $rs['PTOEMI'];
		$this->total = $rs['TOTAL'];
		$this->secuencial = $rs['SECUENCIAL'];
		$this->claveac = $rs['CLAVEAC'];
		$this->numaut = $rs['NUMAUT'];
		$this->descar = $rs['DESCARGADO'];
		$this->revisado = $rs['REVISADO'];
		$this->enviado = $rs['ENVIADO'];
		$this->tries = $rs['TRIES'];
		$this->logEnvio = $rs['LOGENVIO'];
	}

 	public static function getDatos($from, $to, $lim=0, $ruc=''){
 		(empty($_POST['action'])) ? 'default' : $_POST['action'];
 		$ruc=($ruc=='')? '' : substr($ruc, 0, 10);
 		$rucFilter=($ruc=='') ? $ruc: "documento.RUC LIKE '{$ruc}%' AND ";
 		$limiter=($lim==0) ? '': "LIMIT $lim";
 		$con = Conector::getConexion();
 		$query="SELECT
		documento.RUC, documento.FECHA, tipodoc.DESCRIPCION, documento.TOTAL, 
		documento.REVISADO,documento.DESCARGADO, documento.CLAVEAC, documento.ESTAB, 
		documento.PTOEMI, documento.SECUENCIAL, usuario.NOMBRE, documento.UNICO, documento.COD,
		tipodoc.DESCRIPCION
		FROM
			documento,
			usuario,
			tipodoc
		WHERE
		SUBSTR(documento.RUC FROM 1 FOR 10) = SUBSTR(usuario.RUC FROM 1 FOR 10) AND
		documento.COD=tipodoc.COD AND {$rucFilter}
		documento.FECHA BETWEEN '{$from}' AND '{$to}'
		{$limiter}
		";		
 		$res=mysqli_query($con,$query);
		return $res;
 	}

 	public static function lookup($condicion, $orden, $lim=0){
 		$con = Conector::getConexion();
 		$limiter=($lim==0) ? '': "LIMIT $lim";
		$query="SELECT `UNICO`, `COD`, `RUC`, `FECHA`, `ESTAB`, `PTOEMI`, `TOTAL`, `SECUENCIAL`, `CLAVEAC`, `NUMAUT`, `DESCARGADO`, `REVISADO`, `ENVIADO`, COALESCE(TRIES, 0) as TRIES , LOGENVIO, `USERFNX`, `FVISUAL`
		FROM documento 
		WHERE {$condicion} {$orden} {$limiter};";
		$rs=mysqli_query($con, $query);		
		if (mysqli_num_rows($rs)>0) {
			if(mysqli_num_rows($rs)==1){
				$row= mysqli_fetch_array($rs);
				return new Document($row);
			}else{
				$list = array();
				while ($row = mysqli_fetch_array($rs)) {
					array_push($list, new Document($row));
				}
				return $list;
			}			
		}			
		else{
			return false;
		}
		
 	}

 	public function getUser(){
 		$u = User::lookup('RUC', $this->ruc, 1);
 		if ($u){
 			$row=mysqli_fetch_array($u);
 			$usu= new User($row['NOMBRE'], $row['RUC'], $row['EMAIL'], $row['PASS'], $row['ROLE']);
 			$usu->activo=$row['ACTIVO'];
 			return $usu;
 		}  			
 		else{
 			return false;
 		} 			
 	}
 	public function getTipo(){
 		return Tipo::lookup('COD', $this->codigo, 1);
 	}
 	public function setEnviado(){
 		$con = Conector::getConexion();
 		$query = "UPDATE documento SET enviado=1 WHERE UNICO='{$this->unico}'"; 	
 		mysqli_query($con, $query); 		
 	}

 	public function increaseTries(){
 		$con = Conector::getConexion(); 		
 		$query = "UPDATE documento SET TRIES=COALESCE(TRIES, 0)+1 WHERE UNICO='{$this->unico}'"; 	
 		mysqli_query($con, $query);
 	} 
 	public function putLogEnvio($log){
 		$con = Conector::getConexion(); 		
 		$this->logEnvio=$log;
 		$query = "UPDATE documento SET LOGENVIO='{$log}' WHERE UNICO='{$this->unico}'"; 		
 		mysqli_query($con, $query);
 	}
} 
?>