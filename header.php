<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Facturación electrónica SUNAT">
  <meta name="author" content="VFPSTEAM BI SOLUTIONS">
  <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<title>VFPs eDoc : Documentos Electrónicos </title>






<link rel="stylesheet" type="text/css" href="css/bootstrap.css">  
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> 
  <link href="css/blog-post.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/jquery.ui.min.css">
  <link rel="stylesheet" type="text/css" href="css/cupertino/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="css/jqueryui.css">
  <link rel="stylesheet" type="text/css" href="css/dataTables.jqueryui.min.css">
  <link rel="stylesheet" type="text/css" href="css/responsive.jqueryui.min.css">
  <link rel="stylesheet" type="text/css" href="fa/css/font-awesome.min.css">  



 <link rel="stylesheet" type="text/css" href="css/proyecto/daterangepicker.css">


<script type="text/javascript" src="js/proyecto/jquery.min.js"></script>
<script type="text/javascript" src="js/jqueryActualizado/jquery.dataTables.js"></script>

<script type="text/javascript" src="js/jqueryActualizado/moment.js"></script>
<script type="text/javascript" src="js/jqueryActualizado/datetime-moment.js"></script>


<script type="text/javascript" src="js/proyecto/tether.min.js"></script>

<script type="text/javascript" src="js/proyecto/bootstrap.min.js"></script>

<script type="text/javascript" src="js/jqueryActualizado/jquery-ui.js"></script>  
<script type="text/javascript" src="js/jqueryActualizado/jquery-ui.min.js"></script>  

<script type="text/javascript" src="js/jqueryActualizado/bootstrap.js"></script>
<script type="text/javascript" src="js/jqueryActualizado/bootstrap.min.js"></script>

<script type="text/javascript" src="js/jqueryActualizado/dataTables.jqueryui.js"></script>
<script type="text/javascript" src="js/jqueryActualizado/dataTables.jqueryui.min.js"></script>
<script type="text/javascript" src="js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>


<script type="text/javascript" src="js/proyecto/daterangepicker.js"></script>


  <style type="text/css">
    body {
      background-image: url("img/fondo.jpg"); 
      background-color: #cccccc;
      background-position: -100px -100px; 
      background-attachment:fixed;
    }
    .bien{
      color: green;
    }
    .mal{
      color: red;
    }
    .invisible{
      color: white;
    }
    .numeric{
      text-align: right;
    }    
    #formContacto{
      position: fixed; bottom: 0px;
      float: left;
      max-width: 500px;
    }
    .pegado{
      position: fixed;
      bottom: 0px;
      float: left;          
      max-width: 500px;
    }
    .notify{
      position: fixed;
      bottom: 0px;
      right: 0px;      
      max-width: 500px;
    }     
    .footer{
      width: 100%;
      height: 100px;
      color: white;
      margin-bottom: 75px;
      border: 0px rgb(89,89,89) solid;
      border-radius: 0px 0px 50px 50px;
      background-color: #222222;
      -moz-box-shadow:  0px 0px 10px 6px rgb(128,128,128);
      -webkit-box-shadow:  0px 0px 10px 6px rgb(128,128,128);
      box-shadow:  0px 0px 10px 6px rgb(128,128,128);   
    }
    #data-view{
      display: none;
    }
    #img-loading{
      margin:0 auto;
    }
    .centered{
      text-align: center;
    }

    .rowsel{
      background-color: #62BBD9;
    }
  </style>

  
</head>
<body  >
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home"></span>Documentos Electrónicos</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="info.php">Información</a>
                    </li>
                    <li>
                        <a href="acount.php">Cuenta</a>
                    </li>
                    <li>
                      <a href="reporte.php">Consultas</a>
                    </li> 
                    <li>
                      <a href="dashboard.php">Dashboard</a>
                    </li> 
                    <li>
                      <a href="Total Facturas.php">Cantidad de Documentos</a>
                    </li> 

                    <?php if ($_SESSION['role']=='admin') {?>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Opciones <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="settings.php">Configuración</a></li>
                        <li><a href="admins.php">Usuarios Admin.</a></li>                
                        <li><a href="envios.php">Envios Conf.</a></li>
                      </ul>
                    </li>
                    <?php } ?>
                    <li>
                        <a href="logout.php">Logout<span class="glyphicon glyphicon-user"></span></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>