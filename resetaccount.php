<?php  		
	require('consultas.php');
	require('conectar.php');
	require_once('config.php');
	require('user.php');
	require_once 'lib/swift_required.php'; 

	$conex= Conector::getConexion();
	function url(){
	return sprintf(
	  "%s://%s%s",
	  isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
	  $_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI']                      
	);
	}

	function validateUrl($request, $conexion){
		$user=existRuc($conexion, $request['val']);        	

		if ($request['soli']==md5(getNowDate())&& $request['crip']== md5($user->PASS) && $request['crie']== md5($user->EMAIL) ) {
			return true;
		}else{
			return false;
		}
	}

	function getNowDate(){
		date_default_timezone_set('America/Guayaquil');
		return date("Y-m-d");
	}                  

	function generateUrl($pass, $email, $ruc){
		$criterioClave= md5($pass);
		$criterioMail= md5($email); 	
    $criterioFecha= md5(getNowDate());
    return url()."?soli=".$criterioFecha."&&val=".$ruc."&&crip=".$criterioClave."&&crie=".$criterioMail;
	}

	function sendMail($to, $subject, $body, $type=null){ 				
			$transport = Swift_SmtpTransport::newInstance(HOST, PORT, PROT);
		$transport->setUsername(USERNAME);
		$transport->setPassword(PASSWORD);

		// Create the message
		$message = Swift_Message::newInstance();

		$message->setSubject($subject);
		$message->setBody($body);
		$correos=preg_split("[;]", $to->email);
		$recipient=array();				
		if ($type==null) {
			foreach ($correos as $correo) {
				$recipient[$correo]=$to->nombre;						
			}
			$message->setTo($recipient);
			//$message->setTo(array($to->email => $to->nombre));
			$message->setFrom(FROM, EMPRESA);					
		}else{					
			$message->setTo(array(
			FROM => EMPRESA				   	
			));
			$message->setFrom($to->email, $to->nombre);
		}
		
		//$message->attach(Swift_Attachment::fromPath("path/to/file/file.zip"));

		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		if ($mailer->send($message, $failedRecipients)) {					
			return true;
		}else{
			return false;
		}

       
	}
 		if (isset($_POST['sendSoli'])) {
 			if ($row=existRuc($conex, $_POST['ruc'])) {
 				$object = new User($row->NOMBRE, $row->RUC, $row->EMAIL, $row->PASS , $row->ROLE);
 				$url= generateUrl($object->clave, $object->email, $object->ruc);
 				$content="Estimado usuario ".$object->nombre." usted solicitó el restablecimiento de su contraseña. \nPara confirmar esta petición, y establecer una nueva contraseña para su cuenta, por favor vaya a la siguiente dirección de Internet:\n\n".$url."\n\n(Recuerde que este enlace es válido unicamente durante el día que se generó la solicitud)\n\nSi usted no ha solicitado este restablecimiento de contraseña, no necesita realizar ninguna acción y puede eliminar este mensaje.";
 				if(!sendMail($object, 'Restablecimiento de contraseña', $content)){
 					$info= "Lo sentimos no se pudo realizar la accion por favor comuniquese con el administrador del sitio";	
 				}else{
 					$info="<p align='justify'>Le hemos enviado un enlace de restablecimeinto de contraseña, a la dirección de correo electrónico ".$object->email." que tiene registrada en su cuenta, para iniciar sesión clic <a class='btn btn-danger' href='login.php'><i class='glyphicon glyphicon-lock'></i>aqui</a></p>";
 				}				

 			}else{
 				$info="Lo sentimos no se ha encontrado conicidencias con el RUC ingresado para mayor informacion comuniquese con el administrador del sitio";
 			} 			
 		}
 		if (isset($_POST['sendMsg'])) {

 			$asunto=$_POST['asunto'];
 			$mensaje=$_POST['mensaje']; 		
 			$userMensaje= new User($_POST['nombre'],'000000000',$_POST['email'], 'secret', 'aux' );
 			if (sendMail($userMensaje, $asunto, $mensaje,'special')) {
 				$msgConfirm="Su mensaje ha sido enviado exitosamente, clic <a class='btn btn-danger' href='login.php'><i class='glyphicon glyphicon-lock'></i>aqui</a> para salir";
 			}else{
 				$msgConfirm="Lo sentimos no se pudo realizar la accion por favor comuniquese con el administrador del sitio";
 			}
 		}

        

 ?>
<html>
<head>
	<title>Nueva contraseña</title>

		<meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta name="description" content="">
	  <meta name="author" content="">
		<title>Home</title>

		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
    	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 

	
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">  
	  <link href="css/blog-post.css" rel="stylesheet">
	  <link rel="stylesheet" type="text/css" href="css/cupertino/jquery.ui.theme.css">
	  <link rel="stylesheet" type="text/css" href="css/cupertino/jquery-ui.css">

	  <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
	  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
	  <script type="text/javascript" src="js/bootstrap.js"></script>  

	  <style type="text/css">
        #head{
            margin-top: 30px;
            border-radius: 30px;
            border-style: solid;
            border-color: #428BCA;
        }

        #log{            
            border-style: solid;
            border-color: #428BCA;
        }

        body {
          background-image: url("img/fondo.jpg"); 
          background-color: #cccccc;
          background-position: -100px -100px; 
          background-attachment:fixed;
        }
        #formContacto{
	      position: fixed; bottom: 0px;
	      float: left;
	      max-width: 500px;
	    }
	    .pegado{
	      position: fixed;
	      bottom: 0px;
	      float: left;          
	      max-width: 500px;
	    }
	    .notify{          
	      position:absolute;
	      top:10;
	      left:10;
	      max-width: 500px;
	    }	    

    </style>
</head>
<body>

	
	<div class="container">

	<?php if (!$_GET) { //#IF?>

		<div class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-primary">
			<div class="panel-heading"><h4>Cambio de contraseña</h4></div>
				
			<div class="panel-body">

						<form action="resetaccount.php" method="post">
						<div class="form-group">
						<label>RUC/CI:	</label><input class="form-control" type="text" name="ruc" placeholder = "Ingrese su RUC/DNI">
						</div>
						<div class="row">
							<div class="col-md-6"><input name="sendSoli" class="btn btn-info btn-block" type="submit" value="Enviar solicitud"></div>
							
							<div class="col-md-6"><a href="login.php" class="btn btn-danger btn-block">Cancelar</a></div>
						</div>
						
						</form>
			</div>
			</div>
		</div>


	<?php }elseif (validateUrl($_GET, $conex)) {?>
		<div  style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

		<div class="panel panel-primary" id="log" >
                    <div class="panel-heading">
                        <div class="panel-title">Cambio de contraseña personal</div>
                        
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                    	

                    			<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form class="form-horizontal" role="form" action="recuperacion.php" method="POST">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="login-username" type="password" class="form-control" name="pass1" value="" placeholder="Nueva contraseña">                                        
                                        <input type="hidden" name="option" value="1">
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="login-password" type="password" class="form-control" name="pass2" placeholder="Repita su nueva contraseña">
                                        <input type="hidden" name="ruc" value="<?php echo $_GET['val'] ?>">
                                    </div>
                                    

                                
                            	<div class="row">
                            		<div class="col-lg-6"><input type="submit" class="btn btn-block btn-info" value="Enviar"></div>
                            		<div class="col-lg-6"><a class="btn btn-danger btn-block" href="login.php">Cancelar</a></div>
                            	</div>
                                      
                                      
                              
                        </form>     
                        

                        </div>                     
                    </div> 

                 </div> 

	<?php }else{ ?>

		<div class="row">
			<div class="col-lg-4"></div>
				<div class="col-lg-4"><div class="alert alert-warning">Estimado usuario el enlace ya no esta disponible <a class="btn btn-danger" href="login.php"><i class="glyphicon glyphicon-lock"></i>aqui</a></div></div>
				<div class="col-lg-4"></div>
		</div>

	<?php } ?>


	

</div>

<div class="notify pull-rigth">		
		<?php if (isset($info)) { ?>

				<div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-info-circle"></i><?php echo $info; ?>
                </div>				
		<?php } ?>

		<?php if (isset($msgConfirm)) { ?>
				<div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="fa fa-info-circle"></i><?php echo $msgConfirm; ?>
                </div>				
		<?php } ?>
</div>

<div id="formContacto" class="">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<strong>Formulario de contacto</strong>
		</div>
		<div class="panel-body">

			<form  role="form" action="resetaccount.php" method="POST">
                                    
                            	<div class="form-group">
							    <label>Email:</label>
							    <input required name="email" type="email" class="form-control" placeholder="Su email">
								</div>
								
								<div class="form-group">
							    <label >Nombre:</label>
							    <input required name="nombre" type="text" class="form-control" placeholder="Su nombre">
							    </div>

							    <div class="form-group">
							    <label>Asunto:</label>
							    <input required name="asunto" type="text" class="form-control" size="200" placeholder="Asunto">
							    </div>

							    <div class="form-group">
							    <label>Mensaje:</label>
							    <textarea required name="mensaje" class="form-control" placeholder="Detalle del requerimiento o incidencia"></textarea>
							    </div>
                            <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="row">
                                    	<div class="col-lg-6"><input name="sendMsg" type="submit" class="btn btn-info btn-block" value="Enviar mensaje"> </div>
                                    	<div class="col-lg-6"><button type="button" class="btn btn-danger btn-block" id="btnCancelar">Cancelar</button> </div>
                                    </div>
                                </div>                                
                        </form>			
		</div>
	</div>
</div>
<button id="btnContacto" class="pegado btn btn-success btn-block">Enviar mensaje al administrador!</button>


<script>
$(document).ready(function(){
	$( "#formContacto" ).hide();
});
$( "#btnContacto" ).click(function() {
  $( "#formContacto" ).toggle( "slide", 1000 );

});
$( "#btnCancelar" ).click(function() {
  $( "#formContacto" ).toggle( "slide", 1000 );
});
</script>
</body>
</html>

