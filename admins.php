<?php 
session_start();  
  include_once("consultas.php");
  require_once('correos.php');  
  if (!isset($_SESSION['loggedin'])) {   
    header('location: login.php');
    }else{
    include_once 'header.php';
    include_once 'user.php';   
    if ($_POST) {    	
    	switch ($_POST['boton']) {
    		case 'Guardar':
    			if ($_POST['accion']=='create') {
    				$activo=0;
    				$nuevo=  new User($_POST['nom'], $_POST['ruc'], $_POST['email'], $_POST['clave'], $_POST['rol']);
    				if (isset($_POST['activo'])) {
    					$activo=1;
    				}
    				$nuevo->activo=$activo;
    				$nuevo->save();
    			}else{
    				$activo=0;
    				if (isset($_POST['activo'])) {
    					$activo=1;
    				}
    				User::update($_POST['nom'], $_POST['ruc'], $_POST['email'], $_POST['clave'], $_POST['rol'], $activo);
    			}
    			break;
    		case 'Eliminar': 			

    			break;
    		
    		default:
    			# code...
    			break;
    	}
    }
     $admins= User::lookup('ROLE', 'admin');
 ?>
 <div class="container">
 	<div class="row">
            <div class="col-lg-8">
                <h1>Configuración de Cuenta</h1>
                <p class="lead">
                    Bienvenido: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['username'];
                      echo "(".$_SESSION['role'].")";
                     ?>
                  </a>
                </p>
                <p >
                    Ruc: <a href="acount.php"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['ruc'];         

                     ?>
                  </a>
                <p >
                    E-mail: <a href="acount.php"><span class="glyphicon glyphicon-envelope"></span>
                      <?php echo $_SESSION['email']; ?>
                    </a>
                </p>
                <hr>             

            </div>
  
            <div class="col-md-4">
                  <div class="well">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">                    
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="img/vida.software.png" width ="100%" heigth="100%" alt="Chania">
                        </div>

                        <div class="item">
                          <img src="img/banner.jpg" width ="100%" heigth="100%"  alt="Chania">
                        </div>
                        <div class="item">
                          <img src="img/sample1.png" width ="100%" heigth="100%" alt="Flower">
                        </div>
                      </div>
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                </div>                
            </div>
        </div>

        <!--Functional content-->
        <div class="row">
        	<div class="col-lg-6">
        		<div class="panel panel-info">
	        	<div class="panel-heading">
	        		<label>Formulario usuarios</label>
	        	</div>
	        	<div class="panel-body">
	        		<form action="" method="POST">
	        			<input type="hidden" id="txt-accion" name="accion" value="create">
	        			<label>Ruc:</label> <input name="ruc" id="txt-ruc" required type="text" class="form-control">	
	        			<label>Nombre:</label> <input name="nom" id="txt-nom" required type="text" class="form-control">	
	        			<label>Email:</label> <input name="email" id="txt-email" required type="email" class="form-control">	
	        			<label>Clave:</label> <input name="clave" id="txt-clave" required type="password" class="form-control">	
	        			<label>Rol:</label> <input name="rol" id="txt-rol" required type="text" class="form-control">
	        			<label>Activo:</label> <input name="activo" id="txt-activo" type="checkbox" class="form-control">
	        			<br>
	        			<div class="row">	        				
	        				<div class="col-lg-6">
	        					<a class="btn btn-warning btn-block" href="admins.php">Nuevo</a>      					
	        				</div>
	        				<div class="col-lg-6">
	        					<input type="submit" name="boton" value="Guardar" class="btn btn-primary btn-block">	
	        				</div>    				
	        			
	        			</div>
	        		</form>
	        	</div>
	        </div>	
        	</div>
        	<div class="col-lg-6">
        	<div class="panel panel-success">
	        	<div class="panel-heading">
	        		<label>Lista de usuarios</label>
	        	</div>
	        	<div class="panel-body">
	        		<div class="table-responsive">
	        		<table class="table table-bordered table-condensed" id="tbl-admin">
	        			<thead>
	        				<tr>
	        					<th>RUC</th>
	        					<th>NOMBRE</th>
	        					<th>EMAIL</th>
	        					<th>CLAVE</th>
	        					<th>ROl</th>
	        					<th>ACTIVO</th>
	        				</tr>
	        			</thead>
	        			<tbody>
	        				<?php while ($row=mysqli_fetch_array($admins)) { ?>
	        				<tr>
	        					<td><?php echo $row['RUC']; ?></td>
	        					<td><?php echo $row['NOMBRE']; ?></td>
	        					<td><?php echo $row['EMAIL']; ?></td>
	        					<td><?php echo $row['PASS']; ?></td>
	        					<td><?php echo $row['ROLE']; ?></td>
	        					<td><?php echo $row['ACTIVO']; ?></td>
	        				</tr>
	        				<?php } ?>
	        			</tbody>
	        		</table>
	        		</div>
	        	</div>
	        </div>	
        	</div>
        </div>

        <br>
        <br>
        <hr>
        <div class="footer">
        <div class="row">
          <div class="col-lg-4"></div>
          <div class="col-lg-4">
            
            <center><i class="fa fa-map-marker footer-contacts-icon"></i>
                    Copyright &copy; VIDA SOFTWARE </center>
                    <center>2016-<?php echo date("Y") ?></center>
                    <center>Lima-Perú</center>
                    <center>Factura 2.0</center>
            
          </div>
          <div class="col-lg-4"></div>
          
        </div>
                    
      </div>
        
 </div>

 <?php  
 include_once 'footer.php';
}?>