<?php
  require_once('../conectar.php');
  $con = Conector::getConexion();


if($_SERVER['REQUEST_METHOD'] == "PUT"){
  header('Content-type: application/json');
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  " ";
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  " ";

  $result = mysqli_query($con, "SHOW COLUMNS FROM diario where field = 'rucempresa'");
  $exist_rucfield = (mysqli_num_rows($result)>0)?TRUE:FALSE;

  $result = mysqli_query($con, "SHOW COLUMNS FROM oficina where field = 'pre_factura'");
  $exist_prefijo = (mysqli_num_rows($result)>0)?TRUE:FALSE;

  $query = "select fecha, tipo from diario where fecha = '$fecha' and tipo = '$tipo'";
  $rs=mysqli_query($con, $query);  
  if (!mysqli_fetch_array($rs)){
    //no existe, insertar en diario

    if ($exist_rucfield){
      $queryruc = "select ruc from ruc";
      $rsruc = mysqli_query($con, $queryruc);
      while ($rowruc = mysqli_fetch_array($rsruc)){
        $ruc = $rowruc['ruc'];
        $query = "insert into diario (fecha, tipo, estado, actualizado, rucempresa) values('$fecha', 'F', 'A', now(), '$ruc')";
        $rs=mysqli_query($con, $query);  
        $error = mysqli_error($con);

        if (empty($error)){
          $query = "insert into diario (fecha, tipo, estado, actualizado, rucempresa) values('$fecha', 'R', 'A', now(), '$ruc')";
          $rs=mysqli_query($con, $query);  
          $error = mysqli_error($con);
        }

        if (empty($error)){
          $query = "insert into diario (fecha, tipo, estado, actualizado, rucempresa) values('$fecha', 'B', 'A', now(), '$ruc')";
          $rs=mysqli_query($con, $query); 
          $error = mysqli_error($con);
        }

      }
    }
    else {

      $query = "insert into diario (fecha, tipo, estado, actualizado) values('$fecha', 'F', 'A', now())";
      $rs=mysqli_query($con, $query);  
      $error = mysqli_error($con);

      if (empty($error)){
        $query = "insert into diario (fecha, tipo, estado, actualizado) values('$fecha', 'R', 'A', now())";
        $rs=mysqli_query($con, $query);  
        $error = mysqli_error($con);
      }

      if (empty($error)){
        $query = "insert into diario (fecha, tipo, estado, actualizado) values('$fecha', 'B', 'A', now())";
        $rs=mysqli_query($con, $query); 
        $error = mysqli_error($con);
      }

    }

  }
  else
    $error = "Ya existe esta Fecha y Tipo";

  if (empty($error)){
    $query = "select fecha from diario_oficina where fecha = '$fecha'";
    $rs=mysqli_query($con, $query);  
    if (!mysqli_fetch_array($rs)){
        //no existe, insertar en diario oficina
        if ($exist_prefijo)
          $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
            " select '$fecha', 'F', '01', concat(pre_factura, serie_numero), 'A', id from oficina where pertenece_a is null";
        else
          $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
            " select '$fecha', 'F', '01', concat('F', serie_numero), 'A', id from oficina where pertenece_a is null";

        $rs=mysqli_query($con, $query); 
        $error = mysqli_error($con);

        if (empty($error)){
          if ($exist_prefijo)
            $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
              " select '$fecha', 'R', '03', concat(pre_boleta, serie_numero), 'A', id from oficina where pertenece_a is null";
          else
            $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
              " select '$fecha', 'R', '03', concat('B', serie_numero), 'A', id from oficina where pertenece_a is null";

          $rs=mysqli_query($con, $query); 
          $error = mysqli_error($con);
        }

        if (empty($error)){
          if ($exist_prefijo)
            $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
              " select '$fecha', 'B', '01', concat(pre_factura, serie_numero), 'A', id from oficina where pertenece_a is null";
          else
            $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
              " select '$fecha', 'B', '01', concat('F', serie_numero), 'A', id from oficina where pertenece_a is null";

          $rs=mysqli_query($con, $query); 
          $error = mysqli_error($con);
        }

        if (empty($error)){
          if ($exist_prefijo)
            $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
              " select '$fecha', 'B', '03', concat(pre_boleta, serie_numero), 'A', id from oficina where pertenece_a is null";
          else
            $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, estado, oficina)" .
              " select '$fecha', 'B', '03', concat('B', serie_numero), 'A', id from oficina where pertenece_a is null";

          $rs=mysqli_query($con, $query); 
          $error = mysqli_error($con);
        }
    }
  }

  echo $error;

}

?>