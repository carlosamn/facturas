<?php
	require_once('../conectar.php');
	$con = Conector::getConexion();

if($_SERVER['REQUEST_METHOD'] == "GET"){
	header('Content-type: application/json; charset=UTF-8');
	$tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  "F";
	$fecha1 = isset($_GET['fecha1']) ? $_GET['fecha1'] :  " ";
	$fecha2 = isset($_GET['fecha2']) ? $_GET['fecha2'] :  " ";

	$query = "select fecha, tipo, " .
		"case estado when 'A' then 'Activo' when 'L' then 'Leído' when 'E' then 'Error' when 'G' then 'Generar' when 'S' then 'Sunat' end as destado, " .
		"ifnull(observacion, '') as observacion, actualizado, ticket, respuesta, estado";

    $result = mysqli_query($con, "SHOW COLUMNS FROM diario where field = 'rucempresa'");
    $exist_rucfield = (mysqli_num_rows($result)>0)?TRUE:FALSE;

  	if ($exist_rucfield)
  		$query = $query . ", rucempresa";
  	else
  		$query = $query . ", '' as rucempresa";

	$query = $query . " from diario where fecha between '$fecha1' and '$fecha2' and tipo = '$tipo'";

	$rs=mysqli_query($con, $query);
	$list = array();
	while ($row= mysqli_fetch_array($rs))
	{
		//$row_obj = array();
		while($elm=each($row))
		{
			if (is_numeric($elm["key"])){
				unset($row[$elm["key"]]);
			}
		}
		$list[] = $row;
	}

	$error1 = mysqli_error($con);

	//echo json_encode(array("error_cab"=>$error1, "cab"=>$list));

	$query = "select fecha, serie, tdoc, ifnull(nuevo, 0) as nuevo, ifnull(error, 0) as error," .
		" ifnull(leido, 0) as leido, ifnull(ensunat, 0) as ensunat, ifnull(encustodia, 0) as encustodia," .
		" case estado when 'A' then 'Activo' when 'R' then 'Reenviar Docs' when 'L' then 'Leído' " .
		"when 'E' then 'Error' end as destado, ifnull(case when tipo = 'B' then encustodia else enportal end, 0) as portal," .
		" tipo, total, ini, fin, observacion, oficina";

	if ($exist_rucfield)
		$query = $query . ", rucempresa";
	else
		$query = $query . ", '' as rucempresa";

	$query = $query . " from diario_oficina where fecha between '$fecha1' and '$fecha2' and tipo = '$tipo' order by fecha, serie, tdoc";

	$rs=mysqli_query($con, $query);
	$list2 = array();
	while ($row= mysqli_fetch_array($rs))
	{
		//$row_obj = array();
		while($elm=each($row))
		{
			if (is_numeric($elm["key"])){
				unset($row[$elm["key"]]);
			}
		}
		$list2[] = $row;
	}

	$error2 = mysqli_error($con);
	echo json_encode(array("error_cab"=>$error1, "cab"=>$list, "error_det"=>$error2, "det"=>$list2));
}

?>