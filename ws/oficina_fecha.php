<?php
  require_once('../conectar.php');
  $con = Conector::getConexion();

//$data = json_decode(file_get_contents('php://input'), true);
//print_r($data);
//echo $data["operacion"];

if($_SERVER['REQUEST_METHOD'] == "LIST"){
  header('Content-type: application/json');
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  "R";
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  " ";
  $ruc = "";
  $exist_rucfield = false;

  if (isset($_GET['ruc'])){
    $ruc = $_GET['ruc'];
    $result = mysqli_query($con, "SHOW COLUMNS FROM diario where field = 'rucempresa'");
    $exist_rucfield = (mysqli_num_rows($result)>0)?TRUE:FALSE;
  }

  if (!$exist_rucfield){

    $query = "select fechaemision, tipodocumento, tipomoneda, serie, numero, " .
      "concat(serie, '-', numero) as serienumero, totalventa, subtotal, " .
      "totalexoneradas, totalnogravada, otroscargos, totalisc, totaligv, estadoregistro, tipomoneda " .
      " from oficina_fecha where resumenbaja = '$tipo' and fechaemision = '$fecha' and " .
      "(estadodocumento <> 'E' or estadodocumento is null) and anl_rchz is null " .
      " order by serie, numero";

    $rs=mysqli_query($con, $query);
    $list = array();
    while ($row= mysqli_fetch_array($rs))
    {
      //$row_obj = array();
      while($elm=each($row))
      {
        if (is_numeric($elm["key"])){
            unset($row[$elm["key"]]);
        }
      }
      $list[] = $row;
    }

  }
  else
  {

    //Por cada Serie que tegan el ruc
    $queryserie = "select distinct serie from diario_oficina " .
      "where fecha = '$fecha' and tipo = '$tipo' and rucempresa = '$ruc'";

    $rsserie = mysqli_query($con, $queryserie);
    $list = array();
    while ($rowserie = mysqli_fetch_array($rsserie))
    {
      //Por cada serie
      $serie = $rowserie['serie'];
      $query = "select fechaemision, tipodocumento, tipomoneda, serie, numero, " .
      "concat(serie, '-', numero) as serienumero, totalventa, subtotal, " .
      "totalexoneradas, totalnogravada, otroscargos, totalisc, totaligv, estadoregistro, tipomoneda " .
      " from oficina_fecha where resumenbaja = '$tipo' and fechaemision = '$fecha' and " .
      "(estadodocumento <> 'E' or estadodocumento is null) and anl_rchz is null and serie = '$serie'" .
      " order by tipodocumento, serie, numero";

      $rs=mysqli_query($con, $query);
      while ($row= mysqli_fetch_array($rs))
      {
        //$row_obj = array();
        while($elm=each($row))
        {
          if (is_numeric($elm["key"])){
              unset($row[$elm["key"]]);
          }
        }
        $list[] = $row;
      }
    }
  }

  echo json_encode(array("error"=>mysqli_error($con), "result"=>$list));
}

if($_SERVER['REQUEST_METHOD'] == "GET"){
  header('Content-type: application/json');
  $oficina = isset($_GET['oficina']) ? $_GET['oficina'] :  "1";
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  " ";
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  " ";
  $tdoc = isset($_GET['tipodocumento']) ? $_GET['tipodocumento'] :  " ";
  $serie = isset($_GET['serie']) ? $_GET['serie'] :  " ";
  $estado = isset($_GET['estado']) ? $_GET['estado'] :  "";

  $query = "select fechaemision, tipodocumento, serie, numero, totalventa, estadodocumento, mensajeerror, " .
    "estadoregistro, ifnull(fechaproceso, '') as fechaproceso, documentocliente, id_ofi " .
    "from oficina_fecha where id_ofi = $oficina and resumenbaja = '$tipo' and fechaemision = '$fecha' " .
    "and tipodocumento = '$tdoc' and serie = '$serie'";

  if (!empty($estado)){
    $query = $query . " and estadodocumento = '$estado'";
  }

  $rs=mysqli_query($con, $query);
  $list = array();
  while ($row= mysqli_fetch_array($rs))
  {
    //$row_obj = array();
    while($elm=each($row))
    {
      if (is_numeric($elm["key"])){
          unset($row[$elm["key"]]);
      }
    }
    $list[] = $row;
  }
  echo json_encode(array("error"=>mysqli_error($con), "result"=>$list));

}

if($_SERVER['REQUEST_METHOD'] == "PUT"){
  header('Content-type: application/json');
  $set = file_get_contents('php://input');
  $odata = json_decode($set);

  $msg = mysqli_real_escape_string($con, $odata->mensajeerror);
  //$fecest = STR_TO_DATE($odata->fechaestado,'%Y-%m-%d %H:%M:%S');

  if ($odata->tdref){
    //Comprueba si existe referencia
    $result = mysqli_query($con, "SHOW COLUMNS FROM oficina_fecha where field = 'tdref'");
    $exist_field = (mysqli_num_rows($result)>0)?TRUE:FALSE;
    if (!$exist_field){
      mysqli_query($con, "ALTER TABLE oficina_fecha ADD tdref char(2), add serienumeroref char(13)");
      $err = mysqli_error($con);
      if (!empty($err)){
        echo mysqli_error($con);
        return;
      }
    }
  }

  $query = "select numero from oficina_fecha where resumenbaja = '$odata->tipo' and tipodocumento = '$odata->tipodocumento' and serie = '$odata->serie' and numero = '$odata->numero'";

  $rs = mysqli_query($con, $query);
  if (mysqli_fetch_array($rs))
  {
    //Existe => Actualizar
    $query = "update oficina_fecha set estadodocumento = '$odata->estadodocumento', mensajeerror = '$msg', " . 
      "estadoregistro = 'A', estadodocumento = '$odata->estadoregistro', fechaproceso = now(), fechaemision = '$odata->fechaemision' ";
    if ($odata->tdref)
      $query = $query . ", tdref = '$odata->tdref', serienumeroref = '$odata->serienumeroref' ";
    $query = $query . "where tipodocumento = '$odata->tipodocumento' and serie = '$odata->serie' and numero = '$odata->numero' and resumenbaja = '$odata->tipo'";
    $rs = mysqli_query($con, $query);
  }
  else
  {
    //No existe => Insertar
    $query = "insert into oficina_fecha (id_ofi, resumenbaja, fechaemision, tipodocumento, serie, numero, totalventa, subtotal, totalexoneradas, totalnogravada, otroscargos, totalisc, totaligv, estadoRegistro, tipomoneda, estadodocumento, mensajeerror, documentocliente, fechaproceso";
    if ($odata->tdref)
      $query = $query . ", tdref, serienumeroref";
    $query = $query . ") values ($odata->oficina, '$odata->tipo', '$odata->fechaemision', '$odata->tipodocumento', '$odata->serie', '$odata->numero', '$odata->totalventa', '$odata->subtotal', '$odata->totalexoneradas', '$odata->totalnogravada', '0.00', '$odata->totalisc', '$odata->totaligv', 'A', '$odata->tipomoneda', '$odata->estadoregistro', '$msg', '$odata->numerodocumentocliente', now()";
    if ($odata->tdref)
      $query = $query . ", '$odata->tdref', '$odata->serienumeroref'";
    $query = $query . ")";
    $rs = mysqli_query($con, $query); 
  }

  echo mysqli_error($con);
  //echo $query;
  //echo json_encode($odata);
}

if($_SERVER['REQUEST_METHOD'] == "POST"){

  header('Content-type: application/json');
  $set = file_get_contents('php://input');
  $odatalist = json_decode($set);

  $oficina = isset($_GET['oficina']) ? $_GET['oficina'] :  1;
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  " ";

    //Comprueba si existe referencia
  $result = mysqli_query($con, "SHOW COLUMNS FROM oficina_fecha where field = 'tdref'");
  $exist_field = (mysqli_num_rows($result)>0)?TRUE:FALSE;
  if (!$exist_field){
    mysqli_query($con, "ALTER TABLE oficina_fecha ADD tdref char(2), add serienumeroref char(13)");
    $err = mysqli_error($con);
    if (!empty($err)){
      echo mysqli_error($con);
      return;
    }
  }

  foreach ($odatalist as $odata){
    $query = "select numero from oficina_fecha where tipodocumento = '$odata->tipodocumento' and serie = '$odata->serie' and numero = '$odata->numero' and resumenbaja = '$tipo'";
    $rs = mysqli_query($con, $query);
    $msg = mysqli_real_escape_string($con, $odata->mensajeerror);
    //$fecest = STR_TO_DATE($odata->fechaestado,'%Y-%m-%d %H:%M:%S');
    if (mysqli_fetch_array($rs))
    {
      //Existe => Actualizar
      $query = "update oficina_fecha set estadodocumento = 'A', mensajeerror = '$msg', estadoregistro = '$odata->estadoregistro', " .
        "totalventa = '$odata->totalventa', subtotal = '$odata->subtotal', totalexoneradas = '$odata->totalexoneradas', " .
        "totalnogravada = '$odata->totalnogravada', otroscargos = '$odata->otroscargos', totalisc = '$odata->totalisc', " .
        "totaligv = '$odata->totaligv', fechaemision='$odata->fechaemision', documentocliente='$odata->documentocliente' ";

      if ($odata->tipodocafecta)
        $query = $query . ", tdref = '$odata->tipodocafecta', serienumeroref = '$odata->serienumeroafectado' ";

        $query = $query . " where tipodocumento = '$odata->tipodocumento' and serie = '$odata->serie' and numero = '$odata->numero' and resumenbaja = '$tìpo' ";
    }
    else
    {
      //No existe => Insertar
      $query = "insert into oficina_fecha (id_ofi, resumenbaja, fechaemision, tipodocumento, serie, numero, totalventa, subtotal, totalexoneradas, totalnogravada, otroscargos, totalisc, totaligv, estadoRegistro, tipomoneda, estadodocumento, mensajeerror, documentocliente";
      
      if ($odata->tipodocafecta)
        $query = $query . ", tdref, serienumeroref";

      $query = $query . ") values ($oficina, '$tipo', '$odata->fechaemision', '$odata->tipodocumento', '$odata->serie', '$odata->numero', '$odata->totalventa', '$odata->subtotal', '$odata->totalexoneradas', '$odata->totalnogravada', '0.00', '$odata->totalisc', '$odata->totaligv', '$odata->estadoregistro', '$odata->tipomoneda', 'A', '$msg', '$odata->numerodocumentocliente'";
  
      if ($odata->tipodocafecta)
        $query = $query . ", '$odata->tipodocafecta', '$odata->serienumeroafectado'";
  
      $query = $query . ")";
    }
    $rs = mysqli_query($con, $query); 
    $error = mysqli_error($con);
    if (!empty($error)){
      echo $query . "\n\r";
      echo $error . "\n\r";
    }

  }

  //echo json_encode($olist);
  //echo json_encode($odata);
}

?>