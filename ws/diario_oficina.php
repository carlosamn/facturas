<?php
  require_once('../conectar.php');
  $con = Conector::getConexion();

//$data = json_decode(file_get_contents('php://input'), true);
//print_r($data);
//echo $data["operacion"];

if($_SERVER['REQUEST_METHOD'] == "DIF"){
  header('Content-type: application/json');
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  "";
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  "";
  $ruc = isset($_GET['ruc']) ? $_GET['ruc'] :  "";

  $andruc = "";
  if (!empty($ruc)){
    $result = mysqli_query($con, "SHOW COLUMNS FROM diario_oficina where field = 'rucempresa'");
    $exist_rucfield = (mysqli_num_rows($result)>0)?TRUE:FALSE;
    if ($exist_rucfield)
      $andruc = "and rucempresa = '$ruc'";
  }

  $query = "select ifnull(of.tipodocumento, do.tdoc) as tipodocumento, " .
    "ifnull(of.serie, do.serie) as serie, ifnull(of.oficina, do.oficina) as oficina, " .
    "of.cant as cant_of, do.portal as cant_do, do.total from (  " .
      "select tdoc, serie, oficina, case when encustodia > 0 then encustodia else enportal end as portal, " .
      "total from diario_oficina " .
      "where tipo = '$tipo' and fecha = '$fecha' $andruc and (encustodia > 0 or enportal > 0 or total > 0)) do " .
    "left join ( select tipodocumento, serie, id_ofi as oficina, count(*) as cant from oficina_fecha  " .
    "where resumenbaja = '$tipo' and fechaemision = '$fecha' and " .
      "(estadodocumento not in ('N', 'E') or estadodocumento is null) " .
    "group by tipodocumento, serie, id_ofi) of  " .
    "on do.oficina = of.oficina and do.tdoc = of.tipodocumento and do.serie = of.serie " .
    "where of.cant <> do.portal or of.cant <> do.total or of.cant is null";

    $rs=mysqli_query($con, $query);
    $list = array();
    while ($row= mysqli_fetch_array($rs))
    {
      //$row_obj = array();
      while($elm=each($row))
      {
        if (is_numeric($elm["key"])){
          unset($row[$elm["key"]]);
        }
      }
      $list[] = $row;
    }
    echo json_encode(array("error"=>mysqli_error($con), "result"=>$list));
}

if($_SERVER['REQUEST_METHOD'] == "REH"){
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  " ";
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  " ";
  $serie = isset($_GET['serie']) ? $_GET['serie'] :  " ";

  $query = "update diario_oficina set estado = 'A' where fecha = '$fecha' and tipo = '$tipo' and serie = '$serie'";
  $rs = mysqli_query($con, $query);
  echo mysqli_error($con);
}

if($_SERVER['REQUEST_METHOD'] == "PUT"){
  header('Content-type: application/json');
  $set = file_get_contents('php://input');
  $odata = json_decode($set);

  $exist_rucfield = false;

  if ($odata->rucempresa){
    $result = mysqli_query($con, "SHOW COLUMNS FROM diario_oficina where field = 'rucempresa'");
    $exist_rucfield = (mysqli_num_rows($result)>0)?TRUE:FALSE;
  }
  $query = "select fecha, tipo, tdoc, serie from diario_oficina where fecha = '$odata->fecha' and tipo = '$odata->tipo' and tdoc = '$odata->tdoc' and serie = '$odata->serie'";
  $rs = mysqli_query($con, $query);
  if (mysqli_fetch_array($rs))
  {
    //Existe => Actualizar
    $query = "update diario_oficina set nuevo = $odata->nuevo, error = $odata->error, leido = $odata->leido, " .
      "ensunat = $odata->sunat, encustodia = $odata->custodia, enportal = $odata->portal, estado = 'L', " .
      "total = $odata->total, ini = $odata->xini, fin = $odata->xfin ";

    if ($exist_rucfield){
      $query = $query . ", rucempresa = '$odata->rucempresa' ";
    }

    $query = $query . "where fecha = '$odata->fecha' and tipo = '$odata->tipo' and tdoc = '$odata->tdoc' and serie = '$odata->serie'";

    $rs = mysqli_query($con, $query);
  }
  else
  {
    //No existe => Insertar
    $query = "insert into diario_oficina (fecha, tipo, tdoc, serie, oficina, estado, " .
      "nuevo, error, leido, ensunat, encustodia, enportal, total, ini, fin";
    
    if ($exist_rucfield){
      $query = $query . ", rucempresa";
    }

    $query = $query . ") values ('$odata->fecha', '$odata->tipo', '$odata->tdoc', '$odata->serie', $odata->oficina, 'L', " .
      " $odata->nuevo, $odata->error, $odata->leido, $odata->sunat, $odata->custodia, $odata->portal, $odata->total, $odata->xini, $odata->xfin";

    if ($exist_rucfield){
      $query = $query . ", '$odata->rucempresa'";
    }

     $query = $query . ")";

    $rs = mysqli_query($con, $query); 
  }
  echo mysqli_error($con);
}

?>