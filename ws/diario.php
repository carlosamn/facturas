<?php
  require_once('../conectar.php');
  $con = Conector::getConexion();

//$data = json_decode(file_get_contents('php://input'), true);
//print_r($data);
//echo $data["operacion"];

if($_SERVER['REQUEST_METHOD'] == "CORREL"){
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  ""; 
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  ""; 

  $loStrQuery = "select correlativo from diario where fecha = '$fecha' and tipo = '$tipo'";
  $rs=mysqli_query($con, $loStrQuery);
  header('Content-type: application/json');
  $corr = 1;
  if (mysqli_num_rows($rs)>0) {
    $row= mysqli_fetch_array($rs);
    $corr = is_null($row['correlativo']) ? 1 : $row['correlativo'] + 1;
  }
  $loStrQuery = "update diario set correlativo = $corr where fecha = '$fecha' and tipo = '$tipo'";
  $rs=mysqli_query($con, $loStrQuery);
  //echo $loStrQuery . "\n\r";
  echo json_encode(array("error"=>mysqli_error($con), "result"=>$corr));
}

if($_SERVER['REQUEST_METHOD'] == "GET"){
  $estado = isset($_GET['estado']) ? $_GET['estado'] :  ""; 
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  "";
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  "";

  if (!empty($estado))
    $query = "select * from diario where estado = '$estado' and tipo = '$tipo'";
  else
    $query = "select * from diario where fecha = '$fecha' and tipo = '$tipo'";
  $rs=mysqli_query($con, $query);
  header('Content-type: application/json');

  $rs=mysqli_query($con, $query);
  $list = array();
  while ($row= mysqli_fetch_array($rs))
  {
    //$row_obj = array();
    while($elm=each($row))
    {
      if (is_numeric($elm["key"])){
        unset($row[$elm["key"]]);
      }
    }
    $list[] = $row;
  }
  echo json_encode(array("error"=>mysqli_error($con), "result"=>$list));
}

if($_SERVER['REQUEST_METHOD'] == "PUT"){
  $fecha = isset($_GET['fecha']) ? $_GET['fecha'] :  "";
  $tipo = isset($_GET['tipo']) ? $_GET['tipo'] :  "";
  $ruc = isset($_GET['ruc']) ? $_GET['ruc'] : "";

  $set_estado = '';
  $set_ticket = '';
  $set_respuesta = '';
  if (!isset($_GET['ticket'])){
    $set_estado = "estado = 'G',";
  }
  else{
    $ticket = isset($_GET['ticket']) ? $_GET['ticket'] :  "";
    $set_ticket = "ticket = '$ticket',";
  }
  if (isset($_GET['respuesta'])){
    $respuesta = $_GET['respuesta'];
    $set_respuesta = "respuesta = '$respuesta', ";
  }

  //$odata = json_decode($data);
  //STR_TO_DATE($fecha, '%d/%m/%Y %r')
  $query = "update diario set $set_estado $set_ticket $set_respuesta actualizado = now() where fecha = '$fecha' and tipo = '$tipo'";
  if (isset($_GET['ruc']))
    $query = $query . "and rucempresa = '$ruc'";
  $rs = mysqli_query($con, $query);

  header('Content-type: text/html');
  $error = mysqli_error($con);
  echo $error;
}

if($_SERVER['REQUEST_METHOD'] == "POST"){
  header('Content-type: application/json');
  $set = file_get_contents('php://input');
  $odata = json_decode($set);
  $mensaje = mysqli_real_escape_string($con, $odata->mensaje);

  $result = mysqli_query($con, "SHOW COLUMNS FROM diario where field = 'rucempresa'");
  $exist_rucfield = (mysqli_num_rows($result)>0)?TRUE:FALSE;

  $query = "update diario set ";
  if (!empty($odata->estado))
    $query = $query . "estado = '$odata->estado', ";

  $query = $query . "observacion = '$mensaje', actualizado = now(), " . 
    "ticket = '$odata->ticket', respuesta = '$odata->respuesta' " .
    "where fecha = '$odata->fecha' and tipo = '$odata->tipo'";

  if ($exist_rucfield)
    $query = $query . " and rucempresa = '$odata->ruc'";

  $rs = mysqli_query($con, $query);

  $error = mysqli_error($con);
  echo $error;
}

?>