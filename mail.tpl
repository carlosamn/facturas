<table style="margin-left: auto; margin-right: auto; width: 724.183px;" border="1">
<tbody>
<tr>
<td style="width: 714.183px;">
<h3><em><strong>&nbsp;Estimad@ {nombre}, informamos que su comprobante electronico ha sido emitido exitosamente. <br />&nbsp;Para descargar los archivos puede ingresara a:</strong></em></h3>
<h3><em><strong>&nbsp;www.misitioweb.com/facturacion</strong></em></h3>
<h3><em><strong>&nbsp;Para ingresar a la aplicaci&oacute;n web por favor utilice su RUC o DNI&nbsp;como Usuario y Contrase&ntilde;a.<br />&nbsp;Los datos de su comprobante electr&oacute;nico son:<br /></strong></em></h3>
</td>
</tr>
</tbody>
</table>
<h2 style="text-align: center;"><em><strong>&nbsp;</strong></em></h2>
<table style="width: 847px; margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="width: 260px;"><strong>Raz&oacute;n Social:</strong></td>
<td style="width: 571px;">&nbsp;{rsocial}</td>
</tr>
<tr>
<td style="width: 260px;"><strong>RUC:</strong></td>
<td style="width: 571px;">&nbsp;{myruc}</td>
</tr>
<tr>
<td style="width: 260px;">&nbsp;</td>
<td style="width: 571px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 260px;"><strong>Cliente/Proveedor:</strong></td>
<td style="width: 571px;">{nombre}</td>
</tr>
<tr>
<td style="width: 260px;"><strong>Identificaci&oacute;n:</strong></td>
<td style="width: 571px;">{ruc}</td>
</tr>
<tr>
<td style="width: 260px;">&nbsp;</td>
<td style="width: 571px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 260px;"><strong>Ambiente:</strong></td>
<td style="width: 571px;">{ambiente}</td>
</tr>
<tr>
<td style="width: 260px;"><strong>Tipo de Comprobante:</strong></td>
<td style="width: 571px;">{descrip}</td>
</tr>
<tr>
<td style="width: 260px;"><strong>Fecha de Emisi&oacute;n:</strong></td>
<td style="width: 571px;">{fecha}</td>
</tr>
<tr>
<td style="width: 260px;"><strong>N&deg;. de Comprobante:</strong></td>
<td style="width: 571px;">{estab}-{ptoemi}-{secuencial}</td>
</tr>
<tr>
<td style="width: 260px;"><strong>Valor Total:</strong></td>
<td style="width: 571px;">{total}</td>
</tr>
<tr>
<td style="width: 260px;">&nbsp;</td>
<td style="width: 571px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 260px;"><strong>N&deg;. Autorizaci&oacute;n:</strong></td>
<td style="width: 571px;">{numaut}</td>
</tr>
<tr>
<td style="width: 260px;"><strong>Clave de Acceso:</strong></td>
<td style="width: 571px;">{claveac}</td>
</tr>
</tbody>
</table>
<p><em><strong>&nbsp; <br /></strong></em></p>
<table style="margin-left: auto; margin-right: auto; width: 506px; height: 64px;" border="1">
<tbody>
<tr>
<td style="width: 496px;">
<h3>&nbsp; Documento Generado por VIDA SOFTWARE</h3>
</td>
</tr>
</tbody>
</table>