<?php 

  session_start();
  include_once("conectar.php");
  include_once("consultas.php");  
  if (!isset($_SESSION['loggedin'])|| !isset($_GET['doc']) ) {
    header('location: login.php');
  }
  else{
    require_once 'user.php';
    require_once 'header.php';
    $conex=Conector::getConexion();
 ?>
    <div class="container">

        <div class="row">

            <div class="col-lg-8">

                <h1>Documentos Electrónicos</h1>

                <!-- Author -->
                <p class="lead">
                    Bienvenido: <a href="#"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['username'];
                      echo "(".$_SESSION['role'].")";

                     ?>
                  </a>
                </p>
                <p >
                    Ruc: <a href="#"><span class="glyphicon glyphicon-user"></span>
                      <?php 
                      echo $_SESSION['ruc'];         

                     ?>
                  </a>
                </p>

                <?php 
                  if (User::checkPass($_SESSION['ruc'])) {
                    
                 ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Alerta:</strong> Su clave es igual al usuario se recomienda modificarla
                        </div>
                    </div>
                </div>
                <?php } ?>

                <?php 
                  if ( isset($_SESSION['msg']) && $_SESSION['msg']!="") {
                    # code...
                  
                 ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong><?php  echo $_SESSION['msg']; ?></strong> 
                        </div>
                    </div>
                </div>
                <?php } $_SESSION['msg']=""; ?>
                     

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

              
                  <div class="well"> 

                    <!--For Carrousel-->

                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
            
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="img/Fondo_de.jpg" width ="100%" heigth="100%" alt="Chania">
                        </div>

                        <div class="item">
                          <img src="img/banner.jpg" width ="100%" heigth="100%"  alt="Chania">
                        </div>

                        <div class="item">
                          <img src="img/sample1.png" width ="100%" heigth="100%" alt="Flower">
                        </div>

                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>

                   
                </div>
                 
                

            </div>

        </div>
        <!-- /.row -->

        <?php 
          $doc=$_GET['doc'];

          $auth=getDoc($conex,$doc);   
          $auth=$auth.".pdf";
          $root = "pdf/"; 
          $file = basename($auth);
          $path = $root.$file;          
            
          
          

          if (!is_file($path)) {
            # code...

            $auth=getOther($conex,$doc);   
            $auth=$auth.".pdf";
            $root = "pdf/"; 
            $file = basename($auth);
            $path = $root.$file;           
              
          }
          if (is_file($path)) {          

        ?>


        <hr>
        <div class="row">
          <div class="col-lg-12">
                  <div class="well" id="content">
                    <h2>Visor de Documentos</h2>
                                       
                    <object type="application/pdf" data="<?php echo $path; ?>" width="100%" height="500px"></object>
                    
                  </div>
          </div>
        </div>
        <?php
         if ($_SESSION['role']=="user"){
 	    setReviewed($conex,$doc); 
	}
          }else{

         

         ?>
         <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>Alerta:</strong> No Existe el archivo!!
                        </div>
                    </div>
                </div>
        <?php 
         }
         ?>

        <!-- Footer -->
        <hr>
      <div class="footer">
        <div class="row">
          <div class="col-lg-4"></div>
          <div class="col-lg-4">
            
            <center><i class="fa fa-map-marker footer-contacts-icon"></i>
                    Copyright &copy; VIDA SOFTWARE </center>
                    <center>2016-<?php echo date("Y") ?></center>
                    <center>Lima-Perú</center>
                    <center>Factura 2.0</center>
            
          </div>
          <div class="col-lg-4"></div>
          
        </div>
                    
      </div>

    </div>

<?php 
include_once 'footer.php';
} ?>