<?php 
if (!file_exists('config.php')) {
        header('Location: install.php');
} 

require_once('config.php');
require_once 'lib/swift_required.php'; 

function sendMail($to, $subject, $body, $type=null){

 				$transport = Swift_SmtpTransport::newInstance(HOST, PORT, PROT);
				$transport->setUsername(USERNAME);
				$transport->setPassword(PASSWORD);

				// Create the message
				$message = Swift_Message::newInstance();

				$message->setSubject($subject);
				$message->setBody($body);
				$correos=preg_split("[;]", $to->email);
				$recipient=array();				
				if ($type==null) {
					foreach ($correos as $correo) {
						$recipient[$correo]=$to->nombre;						
					}
					$message->setTo($recipient);
					//$message->setTo(array($to->email => $to->nombre));
					$message->setFrom(FROM, EMPRESA);					
				}else{					
					$message->setTo(array(
					FROM => EMPRESA				   	
					));
					foreach ($correos as $correo) {
						$recipient[$correo]=$to->nombre;						
					}
					//var_dump($recipient);
					$message->setFrom($recipient);

				}
				
				//$message->attach(Swift_Attachment::fromPath("path/to/file/file.zip"));

				// Send the email
				$mailer = Swift_Mailer::newInstance($transport);
				if ($mailer->send($message, $failedRecipients)) {					
					return true;
				}else{
					return false;
				}		       
}


 ?>